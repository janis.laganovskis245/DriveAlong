﻿using DriveAlongXamarin.PageModels;
using FreshMvvm;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using DriveAlongXamarin.Global;

namespace DriveAlongXamarin
{
    public partial class App : Application
    {
        public GlobalInfo AppSettings { get; }
        public App()
        {
            InitializeComponent();
            AppSettings = new GlobalInfo();

            var view = FreshPageModelResolver.ResolvePageModel<MainPageModel>();
            var navigationView = new FreshNavigationContainer(view);
            MainPage = navigationView;
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
