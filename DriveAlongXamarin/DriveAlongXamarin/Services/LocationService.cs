﻿using DriveAlongXamarin.Models;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Text.Json;
using DriveAlongXamarin.Constants;
using System.Collections.Generic;
using DriveAlongXamarin.Models.Dtos;

namespace DriveAlongXamarin.Services
{
    public static class LocationService
    {          
        public static async Task<List<LocationDto>> GetLocations()
        {
            var result = new List<LocationDto>();
            try
            {
                var client = BaseService.GetInsecureHandler();

                var response = client.GetAsync($"{ConnectionStrings.ServerAdress}{ConnectionStrings.LocationControllerName}{ConnectionStrings.GetLocationsName}");
                var responseStirng = response.GetAwaiter().GetResult();
                using (HttpContent content = responseStirng.Content)
                {
                    var json = content.ReadAsStringAsync();
                    result = JsonSerializer.Deserialize<List<LocationDto>>(json.Result);
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }
    }
}
