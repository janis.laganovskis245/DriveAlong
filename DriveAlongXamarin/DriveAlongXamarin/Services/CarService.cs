﻿using DriveAlongXamarin.Models;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Text.Json;
using DriveAlongXamarin.Constants;
using System.Collections.Generic;
using DriveAlongXamarin.Models.Dtos;

namespace DriveAlongXamarin.Services
{
    public static class CarService
    {          
        public static async Task<bool> CreateCar(CreateCarDto carData)
        {
            var result = false;
            try
            {
                var client = BaseService.GetInsecureHandler();

                var requestData = JsonSerializer.Serialize(carData);
                HttpContent requestContent = new StringContent(requestData);
                requestContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                var response = client.PostAsync($"{ConnectionStrings.ServerAdress}{ConnectionStrings.CarControllerName}{ConnectionStrings.CreateVehiclenName}", requestContent);
                var responseStirng = response.GetAwaiter().GetResult();
                using (HttpContent content = responseStirng.Content)
                {
                    var json = content.ReadAsStringAsync();
                    result = JsonSerializer.Deserialize<bool>(json.Result);
                }
            }
            catch (Exception ex)
            {
            }
            return result;
        }

        public static async Task<bool> UpdateCar(CarDto carData)
        {
            var result = false;
            try
            {
                var client = BaseService.GetInsecureHandler();

                var requestData = JsonSerializer.Serialize(carData);
                HttpContent requestContent = new StringContent(requestData);
                requestContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                var response = client.PostAsync($"{ConnectionStrings.ServerAdress}{ConnectionStrings.CarControllerName}{ConnectionStrings.UpdateVehicleName}", requestContent);
                var responseStirng = response.GetAwaiter().GetResult();
                using (HttpContent content = responseStirng.Content)
                {
                    var json = content.ReadAsStringAsync();
                    result = JsonSerializer.Deserialize<bool>(json.Result);
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public static async Task<List<CarModel>> GetCars(string email)
        {
            var result = new List<CarModel>();
            try
            {
                var client = BaseService.GetInsecureHandler();

                var requestData = JsonSerializer.Serialize(email);
                HttpContent requestContent = new StringContent(requestData);
                requestContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                var response = client.PostAsync($"{ConnectionStrings.ServerAdress}{ConnectionStrings.CarControllerName}{ConnectionStrings.GetVehiclesName}", requestContent);
                var responseStirng = response.GetAwaiter().GetResult();
                using (HttpContent content = responseStirng.Content)
                {
                    var json = content.ReadAsStringAsync();
                    var resultDto = JsonSerializer.Deserialize<List<CarDto>>(json.Result);
                    foreach (var item in resultDto)
                    {
                        result.Add(new CarModel
                        {
                            Manufacturor = item.manufacturor,
                            Model = item.model,
                            Seats = item.seats,
                            VehicleId = item.vehicleId,
                            Year = item.year
                        });
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public static async Task<bool> DeleteCar(int carId)
        {
            var result = false;
            try
            {
                var client = BaseService.GetInsecureHandler();

                var requestData = JsonSerializer.Serialize(carId);
                HttpContent requestContent = new StringContent(requestData);
                requestContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                var response = client.PostAsync($"{ConnectionStrings.ServerAdress}{ConnectionStrings.CarControllerName}{ConnectionStrings.DeleteVehicleName}", requestContent);
                var responseStirng = response.GetAwaiter().GetResult();
                using (HttpContent content = responseStirng.Content)
                {
                    var json = content.ReadAsStringAsync();
                    result = JsonSerializer.Deserialize<bool>(json.Result);
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

    }
}
