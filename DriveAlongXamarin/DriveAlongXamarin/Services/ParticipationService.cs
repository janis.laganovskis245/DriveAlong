﻿using DriveAlongXamarin.Models;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Text.Json;
using DriveAlongXamarin.Constants;
using System.Collections.Generic;
using DriveAlongXamarin.Models.Dtos;

namespace DriveAlongXamarin.Services
{
    public static class ParticipationService
    {    
        public static async Task<bool> CreateParticipationRequest(CreateParticipationRequestDto data)
        {
            var result = false;
            try
            {
                var client = BaseService.GetInsecureHandler();

                var requestData = JsonSerializer.Serialize(data);
                HttpContent requestContent = new StringContent(requestData);
                requestContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                var response = client.PostAsync($"{ConnectionStrings.ServerAdress}{ConnectionStrings.ParticipationControllerName}{ConnectionStrings.CreateParticipationRequestName}", requestContent);
                var responseStirng = response.GetAwaiter().GetResult();
                using (HttpContent content = responseStirng.Content)
                {
                    var json = content.ReadAsStringAsync();
                    result = JsonSerializer.Deserialize<bool>(json.Result);
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public static async Task<List<ParticipationRequestResponseModel>> GetParticipationRequestsByRace(int raceId)
        {
            var result = new List<ParticipationRequestResponseModel>();
            try
            {
                var client = BaseService.GetInsecureHandler();

                var requestData = JsonSerializer.Serialize(raceId);
                HttpContent requestContent = new StringContent(requestData);
                requestContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                var response = client.PostAsync($"{ConnectionStrings.ServerAdress}{ConnectionStrings.ParticipationControllerName}{ConnectionStrings.GetParticipationRequestsByRaceName}", requestContent);
                var responseStirng = response.GetAwaiter().GetResult();
                using (HttpContent content = responseStirng.Content)
                {
                    var json = content.ReadAsStringAsync();
                    var resultDto = JsonSerializer.Deserialize<List<ParticipationRequestResponseDto>>(json.Result);
                    foreach (var item in resultDto)
                    {
                        result.Add(new ParticipationRequestResponseModel
                        {
                            ParticipationRequestId = item.participationRequestId,
                            RaceId = item.raceId,
                            UserName = item.userName,
                            UserId = item.userId,
                            UserRating = item.userRating
                        });
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return result;
        }

        public static async Task<List<ParticipationRequestResponseModel>> GetParticipationRequestsByUser(string email)
        {
            var result = new List<ParticipationRequestResponseModel>();
            try
            {
                var client = BaseService.GetInsecureHandler();

                var requestData = JsonSerializer.Serialize(email);
                HttpContent requestContent = new StringContent(requestData);
                requestContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                var response = client.PostAsync($"{ConnectionStrings.ServerAdress}{ConnectionStrings.ParticipationControllerName}{ConnectionStrings.GetParticipationRequestsByUserName}", requestContent);
                var responseStirng = response.GetAwaiter().GetResult();
                using (HttpContent content = responseStirng.Content)
                {
                    var json = content.ReadAsStringAsync();
                    var resultDto = JsonSerializer.Deserialize<List<ParticipationRequestResponseDto>>(json.Result);
                    foreach (var item in resultDto)
                    {
                        result.Add(new ParticipationRequestResponseModel
                        {
                            ParticipationRequestId = item.participationRequestId,
                            RaceId = item.raceId,
                            UserName = item.userName,
                            UserId = item.userId,
                            UserRating = item.userRating
                        });
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public static async Task<bool> CreateParticipationDenial(CreateParticipationDenialDto request)
        {
            var result = false;
            try
            {
                var client = BaseService.GetInsecureHandler();

                var requestData = JsonSerializer.Serialize(request);
                HttpContent requestContent = new StringContent(requestData);
                requestContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                var response = client.PostAsync($"{ConnectionStrings.ServerAdress}{ConnectionStrings.ParticipationControllerName}{ConnectionStrings.CreateParticipationDenialName}", requestContent);
                var responseStirng = response.GetAwaiter().GetResult();
                using (HttpContent content = responseStirng.Content)
                {
                    var json = content.ReadAsStringAsync();
                    result = JsonSerializer.Deserialize<bool>(json.Result);
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public static async Task<List<ParticipationDenialResponseModel>> GetParticipationDenialsByUser(string email)
        {
            var result = new List<ParticipationDenialResponseModel>();
            try
            {
                var client = BaseService.GetInsecureHandler();

                var requestData = JsonSerializer.Serialize(email);
                HttpContent requestContent = new StringContent(requestData);
                requestContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                var response = client.PostAsync($"{ConnectionStrings.ServerAdress}{ConnectionStrings.ParticipationControllerName}{ConnectionStrings.GetParticipationDenialsByUserName}", requestContent);
                var responseStirng = response.GetAwaiter().GetResult();
                using (HttpContent content = responseStirng.Content)
                {
                    var json = content.ReadAsStringAsync();
                    var resultDto = JsonSerializer.Deserialize<List<ParticipationDenialResponseDto>>(json.Result);
                    foreach (var item in resultDto)
                    {
                        result.Add(new ParticipationDenialResponseModel
                        {
                           Comment = item.comment,
                           EndLocation = item.endLocation,
                           RaceId = item.raceId,
                           StartDate = item.startDate,
                           StartLocaiton = item.startLocaiton
                        });
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return result;
        }

        public static async Task<List<ParticipationResponseModel>> GetParticipationsByUser(string email)
        {
            var result = new List<ParticipationResponseModel>();
            try
            {
                var client = BaseService.GetInsecureHandler();

                var requestData = JsonSerializer.Serialize(email);
                HttpContent requestContent = new StringContent(requestData);
                requestContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                var response = client.PostAsync($"{ConnectionStrings.ServerAdress}{ConnectionStrings.ParticipationControllerName}{ConnectionStrings.GetParticipationsByUserName}", requestContent);
                var responseStirng = response.GetAwaiter().GetResult();
                using (HttpContent content = responseStirng.Content)
                {
                    var json = content.ReadAsStringAsync();
                    var resultDto = JsonSerializer.Deserialize<List<ParticipationResponseDto>>(json.Result);
                    foreach (var item in resultDto)
                    {
                        result.Add(new ParticipationResponseModel
                        {
                            EndLocation = item.endLocation,
                            RaceId = item.raceId,
                            StartDate = item.startDate,
                            StartLocaiton = item.startLocaiton                          
                        });
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return result;
        }

        public static async Task<bool> CreateParticipation(CreateParticipationDto request)
        {
            var result = false;
            try
            {
                var client = BaseService.GetInsecureHandler();

                var requestData = JsonSerializer.Serialize(request);
                HttpContent requestContent = new StringContent(requestData);
                requestContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                var response = client.PostAsync($"{ConnectionStrings.ServerAdress}{ConnectionStrings.ParticipationControllerName}{ConnectionStrings.CreateParticipationName}", requestContent);
                var responseStirng = response.GetAwaiter().GetResult();
                using (HttpContent content = responseStirng.Content)
                {
                    var json = content.ReadAsStringAsync();
                    result = JsonSerializer.Deserialize<bool>(json.Result);
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }
    }
}
