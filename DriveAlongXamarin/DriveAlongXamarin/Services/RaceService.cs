﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Text.Json;
using DriveAlongXamarin.Constants;
using System.Collections.Generic;
using DriveAlongXamarin.Models.Dtos;

namespace DriveAlongXamarin.Services
{
    public static class RaceService
    {
        public static async Task<bool> CreateRace(CreateRaceDto data)
        {
            var result = false;
            try
            {
                var client = BaseService.GetInsecureHandler();

                var requestData = JsonSerializer.Serialize(data);
                HttpContent requestContent = new StringContent(requestData);
                requestContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                var response = client.PostAsync($"{ConnectionStrings.ServerAdress}{ConnectionStrings.RaceControllerName}{ConnectionStrings.CreateRaceName}", requestContent);
                var responseStirng = response.GetAwaiter().GetResult();
                using (HttpContent content = responseStirng.Content)
                {
                    var json = content.ReadAsStringAsync();
                    result = JsonSerializer.Deserialize<bool>(json.Result);
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public static async Task<List<RaceResponseModel>> GetRacesByLocations(GetRaceByLocationDto data)
        {
            var result = new List<RaceResponseModel>();
            try
            {
                var client = BaseService.GetInsecureHandler();

                var requestData = JsonSerializer.Serialize(data);
                HttpContent requestContent = new StringContent(requestData);
                requestContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                var response = client.PostAsync($"{ConnectionStrings.ServerAdress}{ConnectionStrings.RaceControllerName}{ConnectionStrings.GetRacesByLocationsName}", requestContent);
                var responseStirng = response.GetAwaiter().GetResult();
                using (HttpContent content = responseStirng.Content)
                {
                    var json = content.ReadAsStringAsync();
                    var resultDto = JsonSerializer.Deserialize<List<RaceResponseDto>>(json.Result);
                    foreach (var item in resultDto)
                    {
                        result.Add(new RaceResponseModel
                        {
                            StartLocaiton = item.startLocaiton,
                            DesiredPay = item.desiredPay,
                            StartDate = item.startDate,
                            DriverId = item.driverId,
                            DriverRating = item.driverRating,
                            EndLocation = item.endLocation,
                            FreeSeats = item.freeSeats,
                            RaceId = item.raceId,
                            Stops = item.stops,
                            VehicleManufacturor = item.vehicleManufacturor,
                            VehicleModel = item.vehicleModel,
                            VehicleYear = item.vehicleYear
                        });
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return result;
        }

        public static async Task<List<RaceResponseModel>> GetRacesByUser(GetRaceByUserDto data)
        {
            var result = new List<RaceResponseModel>();
            try
            {
                var client = BaseService.GetInsecureHandler();

                var requestData = JsonSerializer.Serialize(data);
                HttpContent requestContent = new StringContent(requestData);
                requestContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                var response = client.PostAsync($"{ConnectionStrings.ServerAdress}{ConnectionStrings.RaceControllerName}{ConnectionStrings.GetRacesByUserName}", requestContent);
                var responseStirng = response.GetAwaiter().GetResult();
                using (HttpContent content = responseStirng.Content)
                {
                    var json = content.ReadAsStringAsync();
                    var resultDto = JsonSerializer.Deserialize<List<RaceResponseDto>>(json.Result);
                    foreach (var item in resultDto)
                    {
                        result.Add(new RaceResponseModel
                        {
                            StartLocaiton = item.startLocaiton,
                            DesiredPay = item.desiredPay,
                            StartDate = item.startDate,
                            DriverId = item.driverId,
                            DriverRating = item.driverRating,
                            EndLocation = item.endLocation,
                            FreeSeats = item.freeSeats,
                            RaceId = item.raceId,
                            Stops = item.stops,
                            VehicleManufacturor = item.vehicleManufacturor,
                            VehicleModel = item.vehicleModel,
                            VehicleYear = item.vehicleYear
                        });
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return result;
        }

        public static async Task<RaceResponseModel> GetRaceById(int raceId)
        {
            var result = new RaceResponseModel();
            try
            {
                var client = BaseService.GetInsecureHandler();

                var requestData = JsonSerializer.Serialize(raceId);
                HttpContent requestContent = new StringContent(requestData);
                requestContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                var response = client.PostAsync($"{ConnectionStrings.ServerAdress}{ConnectionStrings.RaceControllerName}{ConnectionStrings.GetRaceByIdName}", requestContent);
                var responseStirng = response.GetAwaiter().GetResult();
                using (HttpContent content = responseStirng.Content)
                {
                    var json = content.ReadAsStringAsync();
                    var resultDto = JsonSerializer.Deserialize<RaceResponseDto>(json.Result);
                    result = new RaceResponseModel
                    {
                        StartLocaiton = resultDto.startLocaiton,
                        DesiredPay = resultDto.desiredPay,
                        StartDate = resultDto.startDate,
                        DriverId = resultDto.driverId,
                        DriverRating = resultDto.driverRating,
                        EndLocation = resultDto.endLocation,
                        FreeSeats = resultDto.freeSeats,
                        RaceId = resultDto.raceId,
                        Stops = resultDto.stops,
                        VehicleManufacturor = resultDto.vehicleManufacturor,
                        VehicleModel = resultDto.vehicleModel,
                        VehicleYear = resultDto.vehicleYear
                    };
                }
            }
            catch (Exception ex)
            {
            }
            return result;
        }

    }
}
