﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace DriveAlongXamarin.Services
{
    public static class BaseService
    {
        public static HttpClient GetInsecureHandler()
        {
            HttpClientHandler handler = new HttpClientHandler();
            handler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) =>
            {
                if (cert.Issuer.Equals("CN=localhost"))
                    return true;
                return errors == System.Net.Security.SslPolicyErrors.None;
            };
            var client = new HttpClient(handler);
            return client;
        }

    }
}
