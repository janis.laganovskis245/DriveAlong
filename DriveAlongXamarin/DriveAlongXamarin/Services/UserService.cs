﻿using DriveAlongXamarin.Models;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Text.Json;
using DriveAlongXamarin.Constants;

namespace DriveAlongXamarin.Services
{
    public static class UserService
    {
        public static async Task<string> Register(UserModel userData)
        {
            string jwt = null;
            try
            {
                var client = BaseService.GetInsecureHandler();

                var requestData = JsonSerializer.Serialize(userData);
                HttpContent requestContent = new StringContent(requestData);
                requestContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                var response = client.PostAsync($"{ConnectionStrings.ServerAdress}{ConnectionStrings.UserControllerName}{ConnectionStrings.UserControllerRegisterName}", requestContent);
                var responseStirng = response.GetAwaiter().GetResult();
                using (HttpContent content = responseStirng.Content)
                {
                    var json = content.ReadAsStringAsync();
                    var responseModel = JsonSerializer.Deserialize<ResponseModel>(json.Result);
                    jwt = responseModel.response;
                }
            }
            catch (Exception ex)
            {

            }
            return jwt;
        }

        public static async Task<string> Login(UserLoginModel userData)
        {
            string jwt = null;
            try
            {
                var client = BaseService.GetInsecureHandler();

                var requestData = JsonSerializer.Serialize(userData);
                HttpContent requestContent = new StringContent(requestData);
                requestContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                var response = client.PostAsync($"{ConnectionStrings.ServerAdress}{ConnectionStrings.UserControllerName}{ConnectionStrings.UserControllerLoginName}", requestContent);
                var responseStirng = response.GetAwaiter().GetResult();

                using (HttpContent content = responseStirng.Content)
                {
                    var json = content.ReadAsStringAsync();
                    var responseModel = JsonSerializer.Deserialize<ResponseModel>(json.Result);
                    jwt = responseModel.response;
                }
            } 
            catch (Exception ex)
            {

            }
            return jwt;
        }

        public static async Task<bool> IsDriver(string email)
        {
            var isDriver = false;
            try
            {
                var client = BaseService.GetInsecureHandler();

                var requestData = JsonSerializer.Serialize(email);
                HttpContent requestContent = new StringContent(requestData);
                requestContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                var response = client.PostAsync($"{ConnectionStrings.ServerAdress}{ConnectionStrings.UserControllerName}{ConnectionStrings.UserControllerCheckDriverName}", requestContent);
                var responseStirng = response.GetAwaiter().GetResult();

                using (HttpContent content = responseStirng.Content)
                {
                    var json = content.ReadAsStringAsync();
                    isDriver = JsonSerializer.Deserialize<bool>(json.Result);
                }
            }
            catch (Exception ex)
            {

            }
            return isDriver;
        }

    }
}
