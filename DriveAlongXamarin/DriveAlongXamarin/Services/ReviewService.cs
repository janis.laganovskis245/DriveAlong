﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Text.Json;
using DriveAlongXamarin.Constants;
using System.Collections.Generic;
using DriveAlongXamarin.Models.Dtos;

namespace DriveAlongXamarin.Services
{
    public static class ReviewService
    {
        public static async Task<bool> CreateDriverReview(CreateDriverReviewDto data)
        {
            var result = false;
            try
            {
                var client = BaseService.GetInsecureHandler();

                var requestData = JsonSerializer.Serialize(data);
                HttpContent requestContent = new StringContent(requestData);
                requestContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                var response = client.PostAsync($"{ConnectionStrings.ServerAdress}{ConnectionStrings.ReviewControllerName}{ConnectionStrings.CreateDriverReviewName}", requestContent);
                var responseStirng = response.GetAwaiter().GetResult();
                using (HttpContent content = responseStirng.Content)
                {
                    var json = content.ReadAsStringAsync();
                    result = JsonSerializer.Deserialize<bool>(json.Result);
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public static async Task<bool> CreateUserReview(CreateUserReviewDto data)
        {
            var result = false;
            try
            {
                var client = BaseService.GetInsecureHandler();

                var requestData = JsonSerializer.Serialize(data);
                HttpContent requestContent = new StringContent(requestData);
                requestContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                var response = client.PostAsync($"{ConnectionStrings.ServerAdress}{ConnectionStrings.ReviewControllerName}{ConnectionStrings.CreateUserReviewName}", requestContent);
                var responseStirng = response.GetAwaiter().GetResult();
                using (HttpContent content = responseStirng.Content)
                {
                    var json = content.ReadAsStringAsync();
                    result = JsonSerializer.Deserialize<bool>(json.Result);
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public static async Task<List<DriverReviewResponseModel>> GetDriverReviews(int driverId)
        {
            var result = new List<DriverReviewResponseModel>();
            try
            {
                var client = BaseService.GetInsecureHandler();

                var requestData = JsonSerializer.Serialize(driverId);
                HttpContent requestContent = new StringContent(requestData);
                requestContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                var response = client.PostAsync($"{ConnectionStrings.ServerAdress}{ConnectionStrings.ReviewControllerName}{ConnectionStrings.GetDriverReviewsName}", requestContent);
                var responseStirng = response.GetAwaiter().GetResult();
                using (HttpContent content = responseStirng.Content)
                {
                    var json = content.ReadAsStringAsync();
                    var resultDto = JsonSerializer.Deserialize<List<DriverReviewResponseDto>>(json.Result);
                    foreach (var item in resultDto)
                    {
                        result.Add(new DriverReviewResponseModel
                        {
                            Comment = item.comment,
                            Rating = item.rating,
                            UserName = item.userName
                        });
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return result;
        }

        public static async Task<List<UserReviewResponseModel>> GetUserReviews(int userId)
        {
            var result = new List<UserReviewResponseModel>();
            try
            {
                var client = BaseService.GetInsecureHandler();

                var requestData = JsonSerializer.Serialize(userId);
                HttpContent requestContent = new StringContent(requestData);
                requestContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                var response = client.PostAsync($"{ConnectionStrings.ServerAdress}{ConnectionStrings.ReviewControllerName}{ConnectionStrings.GetUserReviewsName}", requestContent);
                var responseStirng = response.GetAwaiter().GetResult();
                using (HttpContent content = responseStirng.Content)
                {
                    var json = content.ReadAsStringAsync();
                    var resultDto = JsonSerializer.Deserialize<List<UserReviewResponseDto>>(json.Result);
                    foreach (var item in resultDto)
                    {
                        result.Add(new UserReviewResponseModel
                        {
                            Comment = item.comment,
                            Rating = item.rating,
                            DriverName = item.driverName
                        });
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return result;
        }

    }
}
