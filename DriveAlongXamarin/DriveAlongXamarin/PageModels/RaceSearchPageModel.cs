﻿using DriveAlongXamarin.Models.Dtos;
using DriveAlongXamarin.Services;
using FreshMvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using Xamarin.Forms;

namespace DriveAlongXamarin.PageModels
{
    public class RaceSearchPageModel : FreshBasePageModel
    {
        public Command SearchRaceCommand { get; set; }

        private ObservableCollection<RaceResponseModel> _Races;
        public ObservableCollection<RaceResponseModel> Races
        {
            get
            {
                return _Races;
            }
            set
            {
                _Races = value;
                RaisePropertyChanged("Races");
            }
        }

        private LocationDto _StartLocaiton;
        public LocationDto StartLocaiton
        {
            get
            {
                return _StartLocaiton;
            }

            set
            {
                _StartLocaiton = value;
                RaisePropertyChanged("StartLocaiton");
            }
        }

        private LocationDto _EndLocation;
        public LocationDto EndLocation
        {
            get
            {
                return _EndLocation;
            }

            set
            {
                _EndLocation = value;
                RaisePropertyChanged("EndLocation");
            }
        }

        private List<LocationDto> _Locations;
        public List<LocationDto> Locations
        {
            get
            {
                return _Locations;
            }

            set
            {
                _Locations = value;
                RaisePropertyChanged("Locations");
            }
        }

        public RaceSearchPageModel()
        {
            Locations = LocationService.GetLocations().Result;
            SearchRaceCommand = new Command(async () =>
            {
                var sw = new Stopwatch();
                sw.Start();
                var races = await RaceService.GetRacesByLocations(new GetRaceByLocationDto
                {
                    StartLocaitonId = StartLocaiton.locationId,
                    EndLocationId = EndLocation.locationId
                });
                sw.Stop();
                var testTime = sw.ElapsedMilliseconds;

                var racesUsed = races.Take(50);
                foreach (var race in racesUsed)
                {
                    race.ApplicationCommand = new Command(() => CoreMethods.PushPageModel<ParticipationResponsePageModel>(new Tuple<int, bool>(race.RaceId, true), true));
                }
                Races = new ObservableCollection<RaceResponseModel>(racesUsed);
            });
        }
    }
}
