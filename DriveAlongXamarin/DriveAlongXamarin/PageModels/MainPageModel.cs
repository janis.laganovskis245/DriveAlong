﻿using DriveAlongXamarin.Constants;
using FreshMvvm;
using System;
using Xamarin.Forms;

namespace DriveAlongXamarin.PageModels
{
    public class MainPageModel : FreshBasePageModel
    {

        public Command CarsCommand { get; set; }
        public Command RacesCommand { get; set; }
        public Command ReviewsCommand { get; set; }

        private bool firstFire = true;

        private bool _IsDriver;
        public bool IsDriver
        {
            get
            {
                return _IsDriver;
            }

            set
            {
                _IsDriver = value;
                RaisePropertyChanged("IsDriver");
                RaisePropertyChanged("IsNotDriver");
            }
        }

        public MainPageModel() 
        {
            var isDriverSetting = (bool?)((App)App.Current).AppSettings.GetValue(KeyNames.IsDriverName);
            if (isDriverSetting.HasValue)
            {
                IsDriver = isDriverSetting.Value;
            }

            CarsCommand = new Command(() =>
            {
                CoreMethods.PushPageModel<CarsPageModel>();
            });
        
            RacesCommand = new Command(() =>
            {
                CoreMethods.PushPageModel<RacesPageModel>();
            });
            ReviewsCommand = new Command(() =>
            {
                CoreMethods.PushPageModel<ReviewsPageModel>();
            });
        }

        protected override void ViewIsAppearing(object sender, EventArgs e)
        {
            base.ViewIsAppearing(sender, e);

            if (firstFire) 
            {
                var isDriverSetting = (bool?)((App)App.Current).AppSettings.GetValue(KeyNames.IsDriverName);
                if (isDriverSetting == null)
                {
                    CoreMethods.PushPageModel<StartupPageModel>(null, true);
                }
                firstFire = false;
            }
        }

        public override void ReverseInit(object returndData)
        {
            base.ReverseInit(returndData);
            var isDriverSetting = (bool?)((App)App.Current).AppSettings.GetValue(KeyNames.IsDriverName);
            if (isDriverSetting.HasValue)
            {
                IsDriver = isDriverSetting.Value;
            }
        }
    }
}
