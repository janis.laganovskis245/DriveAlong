﻿using DriveAlongXamarin.Constants;
using DriveAlongXamarin.Models;
using DriveAlongXamarin.Services;
using FreshMvvm;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace DriveAlongXamarin.PageModels
{
    public class RegisterPageModel : FreshBasePageModel
    {

        public string Name { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public List<string> DriverTypes { get; set; }
        public string CurrentDriverType { get; set; }
        public Command RegisterCommand { get; set; }

        public RegisterPageModel() 
        {
            DriverTypes = new List<string> { "Driver", "User" };
            CurrentDriverType = DriverTypes.First();

            RegisterCommand = new Command(async () =>
            {
                var userData = new UserModel
                {
                    Email = Email,
                    Name = Name,
                    LastName = LastName,
                    Password = Password,
                    IsDriver = string.Equals(CurrentDriverType, DriverTypes.First())
                };
                var token = await UserService.Register(userData);
                if (!string.IsNullOrEmpty(token))
                {
                    var check = await UserService.IsDriver(Email);
                    ((App)App.Current).AppSettings.SetValue(KeyNames.IsDriverName, check);
                    ((App)App.Current).AppSettings.SetValue(KeyNames.EmailName, Email);

                    await CoreMethods.PopPageModel(true);
                }
            });
        }
    }
}
