﻿using DriveAlongXamarin.Constants;
using DriveAlongXamarin.Models;
using DriveAlongXamarin.Services;
using FreshMvvm;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace DriveAlongXamarin.PageModels
{
    public class LoginPageModel : FreshBasePageModel
    {  
        public string Password { get; set; }
        public string Email { get; set; }
        public Command LoginCommand { get; set; }

        public LoginPageModel() 
        {


            LoginCommand = new Command(async () =>
            {
                var userData = new UserLoginModel
                {
                    Email = Email,
                    Password = Password
                };
                var token = await UserService.Login(userData);
                if (!string.IsNullOrEmpty(token))
                {
                    var check = await UserService.IsDriver(Email);
                    ((App)App.Current).AppSettings.SetValue(KeyNames.IsDriverName, check);
                    ((App)App.Current).AppSettings.SetValue(KeyNames.EmailName, Email);

                    await CoreMethods.PopPageModel(true);
                }
            });
            Password = "Qwerty1@3";
            Email = "test1@test.com";
        }
    }
}
