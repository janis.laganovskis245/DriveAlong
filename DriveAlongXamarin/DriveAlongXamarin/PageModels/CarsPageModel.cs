﻿using DriveAlongXamarin.Constants;
using DriveAlongXamarin.Models;
using DriveAlongXamarin.Services;
using FreshMvvm;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Xamarin.Forms;

namespace DriveAlongXamarin.PageModels
{
    public class CarsPageModel : FreshBasePageModel
    {
        public Command NewCarCommand { get; set; }

        private ObservableCollection<CarModel> _Cars;
        public ObservableCollection<CarModel> Cars
        {
            get
            {
                return _Cars;
            }

            set
            {
                _Cars = value;
                RaisePropertyChanged("Cars");
            }
        }

        public CarsPageModel() 
        {
            InitCars();

            NewCarCommand = new Command(() =>
            {
                CoreMethods.PushPageModel<CarsCreatePageModel>(null, true);
            });
        }

        public override void ReverseInit(object returndData)
        {
            base.ReverseInit(returndData);
            InitCars();
        }

        private void InitCars()
        {
            var carmodels = new List<CarModel>();
            var sw = new Stopwatch();
            sw.Start();
            for (int i = 0; i < 100; i++)
            {
                carmodels = CarService.GetCars(((App)App.Current).AppSettings.GetValue(KeyNames.EmailName).ToString()).Result;
            }
            sw.Stop();
            var testResult = sw.ElapsedMilliseconds;
            Cars = new ObservableCollection<CarModel>(carmodels);
            foreach (var carmodel in Cars)
            {
                carmodel.CarEditCommand = new Command(() =>
                {
                    CoreMethods.PushPageModel<CarsCreatePageModel>(carmodel, true);
                });
                carmodel.CarDeleteCommand = new Command(async () =>
                {
                    await CarService.DeleteCar(carmodel.VehicleId);
                    InitCars();
                });
            }
        }
    }
}
