﻿using DriveAlongXamarin.Constants;
using FreshMvvm;
using System;
using Xamarin.Forms;

namespace DriveAlongXamarin.PageModels
{
    public class StartupPageModel : FreshBasePageModel
    {
        public Command LoginCommand { get; set; }
        public Command RegisterCommand { get; set; }
        public StartupPageModel() 
        {
            RegisterCommand = new Command(() =>
            {
                CoreMethods.PushPageModel<RegisterPageModel>(null, true);
            });
            LoginCommand = new Command(() =>
            {
                CoreMethods.PushPageModel<LoginPageModel>(null, true);
            });
        }

        protected override void ViewIsAppearing(object sender, EventArgs e)
        {
            base.ViewIsAppearing(sender, e);

            var isDriverSetting = (bool?)((App)App.Current).AppSettings.GetValue(KeyNames.IsDriverName);
            if (isDriverSetting.HasValue)
            {
                CoreMethods.PopPageModel(data: true, modal:true);
            }
        }
    }
}
