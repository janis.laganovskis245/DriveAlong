﻿using DriveAlongXamarin.Constants;
using DriveAlongXamarin.Models;
using DriveAlongXamarin.Models.Dtos;
using DriveAlongXamarin.Services;
using FreshMvvm;
using System;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using static System.Net.Mime.MediaTypeNames;

namespace DriveAlongXamarin.PageModels
{
    public class ParticipationRequestPageModel : FreshBasePageModel
    {
        private int _raceId;

        private ObservableCollection<ParticipationRequestResponseModel> _Requests;
        public ObservableCollection<ParticipationRequestResponseModel> Requests
        {
            get
            {
                return _Requests;
            }

            set
            {
                _Requests = value;
                RaisePropertyChanged("Requests");
            }
        }


        public ParticipationRequestPageModel()
        {
            InitRequests();
        }

        public override void ReverseInit(object returndData)
        {
            base.ReverseInit(returndData);
            InitRequests();
        }

        private void InitRequests()
        {
            var requests = ParticipationService.GetParticipationRequestsByRace(_raceId).Result;
            Requests = new ObservableCollection<ParticipationRequestResponseModel>(requests);
            foreach (var request in Requests)
            {
                request.AcceptCommand = new Command(async() =>
                {
                    await ParticipationService.CreateParticipation(new CreateParticipationDto
                    {
                        ParticipationRequestId = request.ParticipationRequestId
                    });
                    InitRequests();
                });
                request.RejectCommand = new Command(() =>
                {
                    CoreMethods.PushPageModel<ParticipationResponsePageModel>(new Tuple<int, bool>(request.ParticipationRequestId, false), true);
                });
            }
        }

        public override void Init(object initData)
        {
            base.Init(initData);
            if (initData is int raceId)
            {
                _raceId = raceId;
                InitRequests();
            }
        }
    }
}
