﻿using DriveAlongXamarin.Constants;
using DriveAlongXamarin.Models;
using DriveAlongXamarin.Models.Dtos;
using DriveAlongXamarin.Services;
using FreshMvvm;
using System.Collections.Generic;
using System;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using static System.Net.Mime.MediaTypeNames;
using System.Linq;

namespace DriveAlongXamarin.PageModels
{
    public class RaceCreatePageModel : FreshBasePageModel
    {
        public Command CreateRaceCommand { get; set; }
        public Command AddStopCommand { get; set; }

        private List<LocationDto> _Locations;
        public List<LocationDto> Locations
        {
            get
            {
                return _Locations;
            }

            set
            {
                _Locations = value;
                RaisePropertyChanged("Locations");
            }
        }

        private LocationDto _StartLocaiton;
        public LocationDto StartLocaiton
        {
            get
            {
                return _StartLocaiton;
            }

            set
            {
                _StartLocaiton = value;
                RaisePropertyChanged("StartLocaiton");
            }
        }


        private LocationDto _EndLocation;
        public LocationDto EndLocation
        {
            get
            {
                return _EndLocation;
            }

            set
            {
                _EndLocation = value;
                RaisePropertyChanged("EndLocation");
            }
        }

        private LocationDto _SelectedStopLocation;
        public LocationDto SelectedStopLocation
        {
            get
            {
                return _SelectedStopLocation;
            }

            set
            {
                _SelectedStopLocation = value;
                RaisePropertyChanged("SelectedStopLocation");
            }
        }


        private ObservableCollection<LocationDto> _StopLocations;
        public ObservableCollection<LocationDto> StopLocations
        {
            get
            {
                return _StopLocations;
            }

            set
            {
                _StopLocations = value;
                RaisePropertyChanged("StopLocations");
            }
        }


        private string _DesiredPay;
        public string DesiredPay
        {
            get
            {
                return _DesiredPay;
            }

            set
            {
                _DesiredPay = value;
                RaisePropertyChanged("DesiredPay");
            }
        }

        private DateTime _StartDate;
        public DateTime StartDate
        {
            get
            {
                return _StartDate;
            }

            set
            {
                _StartDate = value;
                RaisePropertyChanged("StartDate");
            }
        }

        private CarModel _CurrentVehicle;
        public CarModel CurrentVehicle
        {
            get
            {
                return _CurrentVehicle;
            }

            set
            {
                _CurrentVehicle = value;
                RaisePropertyChanged("CurrentVehicle");
            }
        }

        private List<CarModel> _Vehicles;
        public List<CarModel> Vehicles
        {
            get
            {
                return _Vehicles;
            }

            set
            {
                _Vehicles = value;
                RaisePropertyChanged("Vehicles");
            }
        }

        private string _AlreadyTakenSeats;
        public string AlreadyTakenSeats
        {
            get
            {
                return _AlreadyTakenSeats;
            }

            set
            {
                _AlreadyTakenSeats = value;
                RaisePropertyChanged("AlreadyTakenSeats");
            }
        }

        public RaceCreatePageModel()
        {
            Locations = LocationService.GetLocations().Result;
            Vehicles = CarService.GetCars(((App)App.Current).AppSettings.GetValue(KeyNames.EmailName).ToString()).Result;
            StartDate = DateTime.Today;
            StopLocations = new ObservableCollection<LocationDto>();
            AddStopCommand = new Command( ()=>
            {
                SelectedStopLocation.RemoveLocationCommand = new Command(() => { StopLocations.Remove(SelectedStopLocation); });
                StopLocations.Add(SelectedStopLocation);
            });

            CreateRaceCommand = new Command(async() =>
            {
                await RaceService.CreateRace(new Models.Dtos.CreateRaceDto
                {
                    Email = ((App)App.Current).AppSettings.GetValue(KeyNames.EmailName).ToString(),
                    AlreadyTakenSeats = int.Parse(AlreadyTakenSeats),
                    DesiredPay = decimal.Parse(DesiredPay),
                    EndLocationId = EndLocation.locationId,
                    StartDate = StartDate,
                    StartLocaitonId = StartLocaiton.locationId,
                    VehicleId = CurrentVehicle.VehicleId,
                    StopLocationIds = StopLocations.Select(x => x.locationId).ToList()                    
                });
                await CoreMethods.PopPageModel(1, true);
            });
        }
    }
}
