﻿using DriveAlongXamarin.Constants;
using DriveAlongXamarin.Models;
using DriveAlongXamarin.Models.Dtos;
using DriveAlongXamarin.Services;
using FreshMvvm;
using System;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using static System.Net.Mime.MediaTypeNames;

namespace DriveAlongXamarin.PageModels
{
    public class ParticipationResponsePageModel : FreshBasePageModel
    {
        private int _requestId;
        private bool _isRequest;

        public Command ConfirmCommand { get; set; }

        private string _Comment;
        public string Comment
        {
            get
            {
                return _Comment;
            }

            set
            {
                _Comment = value;
                RaisePropertyChanged("Comment");
            }
        }


        public ParticipationResponsePageModel()
        {
            ConfirmCommand = new Command(async () =>
            {
                if (_isRequest)
                {
                    await ParticipationService.CreateParticipationRequest(new Models.Dtos.CreateParticipationRequestDto
                    {
                        RaceId = _requestId,
                        Comment = Comment,
                        Email = ((App)App.Current).AppSettings.GetValue(KeyNames.EmailName).ToString()
                    });
                }
                else
                {
                    await ParticipationService.CreateParticipationDenial(new Models.Dtos.CreateParticipationDenialDto
                    {
                        ParticipationRequestId = _requestId,
                        Comment = Comment
                    });
                }
                await CoreMethods.PopPageModel(1, true);
            });
        }

        public override void Init(object initData)
        {
            base.Init(initData);
            if (initData is Tuple<int, bool> request)
            {
                _requestId = request.Item1;
                _isRequest = request.Item2;
            }
        }
    }
}
