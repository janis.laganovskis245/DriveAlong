﻿using DriveAlongXamarin.Constants;
using DriveAlongXamarin.Models;
using DriveAlongXamarin.Services;
using FreshMvvm;
using Xamarin.Forms;

namespace DriveAlongXamarin.PageModels
{
    public class CarsCreatePageModel : FreshBasePageModel
    {
        private int? ExistingCarId;
        public Command SaveCommand { get; set; }

        private string _Manufacturor;
        public string Manufacturor
        {
            get
            {
                return _Manufacturor;
            }

            set
            {
                _Manufacturor = value;
                RaisePropertyChanged("Manufacturor");
            }
        }

        private string _Model;
        public string Model
        {
            get
            {
                return _Model;
            }

            set
            {
                _Model = value;
                RaisePropertyChanged("Model");
            }
        }

        private string _Seats;
        public string Seats
        {
            get
            {
                return _Seats;
            }

            set
            {
                _Seats = value;
                RaisePropertyChanged("Seats");
            }
        }

        private string _Year;
        public string Year
        {
            get
            {
                return _Year;
            }

            set
            {
                _Year = value;
                RaisePropertyChanged("Year");
            }
        }


        public CarsCreatePageModel() 
        {
            SaveCommand = new Command( async() =>
            {
               
                if (ExistingCarId.HasValue)
                {
                    await CarService.UpdateCar(new Models.Dtos.CarDto
                    {
                        manufacturor = Manufacturor,
                        model = Model,
                        seats = int.Parse(Seats),
                        vehicleId = ExistingCarId.Value,
                        year = int.Parse(Year)
                    });
                }
                else
                {
                    await CarService.CreateCar(new Models.Dtos.CreateCarDto
                    {
                        Manufacturor = Manufacturor,
                        Model = Model,
                        Seats = int.Parse(Seats),
                        Year = int.Parse(Year),
                        Email = ((App)App.Current).AppSettings.GetValue(KeyNames.EmailName).ToString()
                    });
                }
                await CoreMethods.PopPageModel(1,true);
            });
        }

        public override void Init(object initData)
        {
            base.Init(initData);
            if (initData is CarModel car)
            {
                ExistingCarId = car.VehicleId;
                Year = car.Year.ToString();
                Model = car.Model;
                Manufacturor = car.Manufacturor;
                Seats = car.Seats.ToString();
            }
        }
    }
}
