﻿using DriveAlongXamarin.Constants;
using DriveAlongXamarin.Models;
using DriveAlongXamarin.Models.Dtos;
using DriveAlongXamarin.Pages;
using DriveAlongXamarin.Services;
using FreshMvvm;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using static System.Net.Mime.MediaTypeNames;

namespace DriveAlongXamarin.PageModels
{
    public class ReviewsPageModel : FreshBasePageModel
    {
        public Command NewRaceCommand { get; set; }
        public Command SearchRacesCommand { get; set; }

        private ObservableCollection<RaceResponseModel> _Races;
        public ObservableCollection<RaceResponseModel> Races
        {
            get
            {
                return _Races;
            }

            set
            {
                _Races = value;
                RaisePropertyChanged("Races");
            }
        }

        private bool _IsDriver;
        public bool IsDriver
        {
            get
            {
                return _IsDriver;
            }

            set
            {
                _IsDriver = value;
                RaisePropertyChanged("IsDriver");
                RaisePropertyChanged("IsNotDriver");

            }
        }

        public bool IsNotDriver
        {
            get
            {
                return !_IsDriver;
            }
        }


        public ReviewsPageModel()
        {
            var isDriverSetting = (bool?)((App)App.Current).AppSettings.GetValue(KeyNames.IsDriverName);
            if (isDriverSetting.HasValue)
            {
                IsDriver = isDriverSetting.Value;
            }
            InitRaces();

            NewRaceCommand = new Command(() =>
            {
                CoreMethods.PushPageModel<RaceCreatePageModel>(null, true);
            });
            SearchRacesCommand = new Command(() =>
            {
                CoreMethods.PushPageModel<RaceSearchPageModel>(null, false);
            });
        }

        public override void ReverseInit(object returndData)
        {
            base.ReverseInit(returndData);
            InitRaces();
        }

        private void InitRaces()
        {
            var racemodels = RaceService.GetRacesByUser( new GetRaceByUserDto
            {
                IsDriver = IsDriver,
                Email = ((App)App.Current).AppSettings.GetValue(KeyNames.EmailName).ToString()
            }).Result;
            Races = new ObservableCollection<RaceResponseModel>(racemodels);
            foreach (var racemodel in Races)
            {
                racemodel.ApplicationCommand = new Command(() =>
                {
                    CoreMethods.PushPageModel<ParticipationRequestPageModel>(racemodel.RaceId, false);
                });
            }
        }
    }
}
