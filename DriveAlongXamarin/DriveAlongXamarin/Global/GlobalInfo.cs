﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DriveAlongXamarin.Global
{
    public class GlobalInfo
    {
        private Dictionary<string, object> _currentInfo;
 
        public GlobalInfo() 
        {
            _currentInfo = new Dictionary<string, object>();
        }

        public object GetValue(string key)
            => _currentInfo.ContainsKey(key) ? _currentInfo[key] : null;
       

        public void SetValue(string key, object value)
        {
            if (_currentInfo.ContainsKey(key))
                _currentInfo[key] = value;
            else
                _currentInfo.Add(key, value);
        }
    }
}
