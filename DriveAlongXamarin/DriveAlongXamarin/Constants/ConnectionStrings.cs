﻿using System;
using System.Collections.Generic;
using System.Text;
using static System.Net.WebRequestMethods;

namespace DriveAlongXamarin.Constants
{
    public static class ConnectionStrings
    {
        public const string ServerAdress = "https://drivealongv1.azurewebsites.net/"; //Location/InsertLocations //https://10.0.2.2:7244/ => https://localhost:7244/ https://drivealongv1.azurewebsites.net/ => live

        public const string UserControllerName = "User/";
        public const string UserControllerLoginName = "Login";
        public const string UserControllerRegisterName = "Register";
        public const string UserControllerDeleteName = "DeleteUser";
        public const string UserControllerCheckDriverName = "IsDriver";

        public const string CarControllerName = "Vehicle/";
        public const string CreateVehiclenName = "CreateVehicle";
        public const string UpdateVehicleName = "UpdateVehicle";
        public const string GetVehiclesName = "GetVehicles";
        public const string DeleteVehicleName = "DeleteVehicle";

        public const string LocationControllerName = "Location/";
        public const string GetLocationsName = "GetLocations"; 

        public const string ParticipationControllerName = "Participation/";
        public const string CreateParticipationRequestName = "CreateParticipationRequest";
        public const string GetParticipationRequestsByRaceName = "GetParticipationRequestsByRace";
        public const string GetParticipationRequestsByUserName = "GetParticipationRequestsByUser";
        public const string CreateParticipationDenialName = "CreateParticipationDenial";
        public const string GetParticipationDenialsByUserName = "GetParticipationDenialsByUser";
        public const string CreateParticipationName = "CreateParticipation";
        public const string GetParticipationsByUserName = "GetParticipationsByUser"; 

        public const string RaceControllerName = "Race/";
        public const string CreateRaceName = "CreateRace";
        public const string GetRacesByLocationsName = "GetRacesByLocations";
        public const string GetRacesByUserName = "GetRacesByUser";
        public const string GetRaceByIdName = "GetRaceById";

        public const string ReviewControllerName = "Review/";
        public const string CreateDriverReviewName = "CreateDriverReview";
        public const string GetDriverReviewsName = "GetDriverReviews";
        public const string CreateUserReviewName = "CreateUserReview";
        public const string GetUserReviewsName = "GetUserReviews";
        //Review
    }
}
