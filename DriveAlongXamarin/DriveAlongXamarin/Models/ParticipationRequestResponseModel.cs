﻿using Xamarin.Forms;

namespace DriveAlongXamarin.Models.Dtos
{
    public class ParticipationRequestResponseModel
    {
        public int ParticipationRequestId { get; set; }
        public int RaceId { get; set; }
        public string UserName { get; set; }
        public int UserId { get; set; }
        public decimal UserRating { get; set; }
        public Command AcceptCommand { get; set; }
        public Command RejectCommand { get; set; }
    }
}
