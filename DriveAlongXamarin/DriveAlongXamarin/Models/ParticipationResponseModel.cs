﻿using System;

namespace DriveAlongXamarin.Models.Dtos
{
    public class ParticipationResponseModel
    {
        public int RaceId { get; set; }
        public LocationDto StartLocaiton { get; set; }
        public LocationDto EndLocation { get; set; }
        public DateTime StartDate { get; set; }
    }
}
