﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace DriveAlongXamarin.Models.Dtos
{
    public class RaceResponseModel
    {
        public LocationDto StartLocaiton { get; set; }
        public LocationDto EndLocation { get; set; }
        public DateTime StartDate { get; set; }
        public decimal DesiredPay { get; set; }
        public int FreeSeats { get; set; }
        public string VehicleManufacturor { get; set; }
        public string VehicleModel { get; set; }
        public int VehicleYear { get; set; }
        public decimal DriverRating { get; set; }
        public List<LocationDto> Stops { get; set; }
        public int RaceId { get; set; }
        public int DriverId { get; set; }
        public bool IsDriver { get; set; }
        public Command ApplicationCommand { get; set; }
    }
}
