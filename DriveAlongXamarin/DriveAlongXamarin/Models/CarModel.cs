﻿using Xamarin.Forms;

namespace DriveAlongXamarin.Models
{
    public class CarModel
    {
        public string Manufacturor { get; set; }
        public string Model { get; set; }
        public int Seats { get; set; }
        public int Year { get; set; }
        public int VehicleId { get; set; }
        public Command CarEditCommand { get; set; }
        public Command CarDeleteCommand { get; set; }

        public string ConcatName => $"{Manufacturor} - {Model}";
    }
}
