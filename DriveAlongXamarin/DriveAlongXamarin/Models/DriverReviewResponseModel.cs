﻿namespace DriveAlongXamarin.Models.Dtos
{
    public class DriverReviewResponseModel
    {
        public string UserName { get; set; }
        public int Rating { get; set; }
        public string Comment { get; set; }
    }
}
