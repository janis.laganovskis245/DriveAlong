﻿namespace DriveAlongXamarin.Models.Dtos
{
    public class DriverReviewResponseDto
    {
        public string userName { get; set; }
        public int rating { get; set; }
        public string comment { get; set; }
    }
}
