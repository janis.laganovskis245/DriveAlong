﻿namespace DriveAlongXamarin.Models.Dtos
{
    public class CreateDriverReviewDto
    {
        public string Email { get; set; }
        public int RaceId { get; set; }
        public int Rating { get; set; }
        public string Comment { get; set; }
    }
}
