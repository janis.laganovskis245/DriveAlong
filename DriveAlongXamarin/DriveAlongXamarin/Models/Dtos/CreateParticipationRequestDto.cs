﻿namespace DriveAlongXamarin.Models.Dtos
{
    public class CreateParticipationRequestDto
    {
        public string Email { get; set; }
        public int RaceId { get; set; }
        public string Comment { get; set; }
    }
}
