﻿namespace DriveAlongXamarin.Models.Dtos
{
    public class UserReviewResponseDto
    {
        public string driverName { get; set; }
        public int rating { get; set; }
        public string comment { get; set; }
    }
}
