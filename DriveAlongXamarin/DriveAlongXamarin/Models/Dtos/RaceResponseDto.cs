﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DriveAlongXamarin.Models.Dtos
{
    public class RaceResponseDto
    {
        public LocationDto startLocaiton { get; set; }
        public LocationDto endLocation { get; set; }
        public DateTime startDate { get; set; }
        public decimal desiredPay { get; set; }
        public int freeSeats { get; set; }
        public string vehicleManufacturor { get; set; }
        public string vehicleModel { get; set; }
        public int vehicleYear { get; set; }
        public decimal driverRating { get; set; }
        public List<LocationDto> stops { get; set; }
        public int raceId { get; set; }
        public int driverId { get; set; }
    }
}
