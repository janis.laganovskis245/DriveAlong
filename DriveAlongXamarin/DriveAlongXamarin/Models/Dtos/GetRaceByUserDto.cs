﻿namespace DriveAlongXamarin.Models.Dtos
{
    public class GetRaceByUserDto
    {
        public string Email { get; set; }
        public bool IsDriver { get; set; }
    }
}
