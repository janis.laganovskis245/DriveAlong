﻿using Xamarin.Forms;

namespace DriveAlongXamarin.Models.Dtos
{
    public class LocationDto
    {
        public int locationId { get; set; }
        public string locationName { get; set; }
        public Command RemoveLocationCommand { get; set; }
    }
}
