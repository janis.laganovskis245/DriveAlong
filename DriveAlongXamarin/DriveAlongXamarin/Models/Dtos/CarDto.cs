﻿namespace DriveAlongXamarin.Models.Dtos
{
    public class CarDto
    {
        public string manufacturor { get; set; }
        public string model { get; set; }
        public int seats { get; set; }
        public int year { get; set; }
        public int vehicleId { get; set; }
    }
}
