﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DriveAlongXamarin.Models.Dtos
{
    public class CreateRaceDto
    {
        public string Email { get; set; }
        public int VehicleId { get; set; }
        public int AlreadyTakenSeats { get; set; }
        public int StartLocaitonId { get; set; }
        public int EndLocationId { get; set; }
        public DateTime StartDate { get; set; }
        public decimal DesiredPay { get; set; }
        public List<int> StopLocationIds { get; set; }
    }
}
