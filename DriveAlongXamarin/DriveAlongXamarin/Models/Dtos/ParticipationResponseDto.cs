﻿using System;

namespace DriveAlongXamarin.Models.Dtos
{
    public class ParticipationResponseDto
    {
        public int raceId { get; set; }
        public LocationDto startLocaiton { get; set; }
        public LocationDto endLocation { get; set; }
        public DateTime startDate { get; set; }
    }
}
