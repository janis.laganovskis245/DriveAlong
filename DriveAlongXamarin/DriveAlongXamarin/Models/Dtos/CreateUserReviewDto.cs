﻿namespace DriveAlongXamarin.Models.Dtos
{
    public class CreateUserReviewDto
    {
        public string Email { get; set; }
        public int ParticipationId { get; set; }
        public int Rating { get; set; }
        public string Comment { get; set; }
    }
}
