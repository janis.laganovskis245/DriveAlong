﻿namespace DriveAlongXamarin.Models.Dtos
{
    public class ParticipationRequestResponseDto
    {
        public int participationRequestId { get; set; }
        public int raceId { get; set; }
        public string userName { get; set; }
        public int userId { get; set; }
        public decimal userRating { get; set; }
    }
}
