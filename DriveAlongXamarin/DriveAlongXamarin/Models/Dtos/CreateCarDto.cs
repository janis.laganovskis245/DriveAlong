﻿namespace DriveAlongXamarin.Models.Dtos
{
    public class CreateCarDto
    {
        public string Manufacturor { get; set; }
        public string Model { get; set; }
        public int Seats { get; set; }
        public int Year { get; set; }
        public string Email { get; set; }
    }
}
