﻿using System;

namespace DriveAlongXamarin.Models.Dtos
{
    public class ParticipationDenialResponseDto
    {
        public int raceId { get; set; }
        public string comment { get; set; }
        public LocationDto startLocaiton { get; set; }
        public LocationDto endLocation { get; set; }
        public DateTime startDate { get; set; }
    }
}
