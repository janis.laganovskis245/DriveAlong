﻿namespace DriveAlongXamarin.Models.Dtos
{
    public class CreateParticipationDenialDto
    {
        public int ParticipationRequestId { get; set; }
        public string Comment { get; set; }
    }
}
