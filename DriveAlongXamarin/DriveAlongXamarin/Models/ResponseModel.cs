﻿using System.Collections.Generic;

namespace DriveAlongXamarin.Models
{
    public class ResponseModel
    {
        public bool isSuccess { get; set; }
        public string response { get; set; }
        public List<string> errors { get; set; }
    }
}
