﻿using System;

namespace DriveAlongXamarin.Models.Dtos
{
    public class ParticipationDenialResponseModel
    {
        public int RaceId { get; set; }
        public string Comment { get; set; }
        public LocationDto StartLocaiton { get; set; }
        public LocationDto EndLocation { get; set; }
        public DateTime StartDate { get; set; }
    }
}
