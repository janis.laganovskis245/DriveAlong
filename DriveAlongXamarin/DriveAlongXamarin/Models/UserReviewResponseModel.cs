﻿namespace DriveAlongXamarin.Models.Dtos
{
    public class UserReviewResponseModel
    {
        public string DriverName { get; set; }
        public int Rating { get; set; }
        public string Comment { get; set; }
    }
}
