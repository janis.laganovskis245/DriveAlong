import { createStackNavigator } from "react-navigation-stack";
import { createAppContainer } from "react-navigation";
import Home from "../screens/home";
import LoginScreen from "../screens/login";
import LoginRegisterScreen from "../screens/loginRegister";
import RegisterScreen from "../screens/register";


const screens = {
    Home: {
        screen: Home
    },
    LoginRegister : {
        screen: LoginRegisterScreen
    },
    Login : {
        screen: LoginScreen
    },
    Register : {
        screen: RegisterScreen
    },

}
const HomeStack = createStackNavigator(screens);

export default createAppContainer(HomeStack);