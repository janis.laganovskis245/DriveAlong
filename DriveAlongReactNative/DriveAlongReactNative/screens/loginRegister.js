import { StyleSheet, View, Button } from 'react-native';


export default function LoginRegisterScreen({ navigation}) { 
    const loginHandler = async () =>{
        navigation.navigate('Login');
    }
    const registerHandler = async () =>{
        navigation.navigate('Register');
    }
    return (
      <View style={styles.container}>
        <Button title='login' onPress={loginHandler}/>
        <Button title='register' onPress={registerHandler}/>
      </View>
    );
  }
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    textInput:{
        borderWidth: 1,
        borderColor: '#999',
        width: 250,
        margin: 15
    }
  });