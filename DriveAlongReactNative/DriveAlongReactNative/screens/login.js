import { useState } from 'react';
import { StyleSheet, Text, View, Button, TextInput } from 'react-native';
import { loginApiCall, isDriverApiCall } from '../services/loginService';

export default function LoginScreen({ navigation}) {
    var startTime = performance.now();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const loginHandler = async () =>{
        const result = await loginApiCall(email, password);
        if(result){
            global.IsloggedIn = true;
            const isDriver = await isDriverApiCall(email);
            global.IsDriver = isDriver;
            global.Email = email;
            navigation.navigate('Home');
        };
    }
    
    const view = (
      <View style={styles.container}>
        <Text>Email:</Text>
        <TextInput style= {styles.textInput} onChangeText={(value)=> setEmail(value)}/> 
        <Text>Password:</Text>
        <TextInput style= {styles.textInput} onChangeText={(value)=> setPassword(value)}/> 
        <Button title='login' onPress={loginHandler}/>
      </View>
    );
    var endTime = performance.now()
    console.log(`Call to doSomething took ${endTime - startTime} milliseconds.`)
    return view;
  }
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    textInput:{
        borderWidth: 1,
        borderColor: '#999',
        width: 250,
        margin: 15
    }
  });