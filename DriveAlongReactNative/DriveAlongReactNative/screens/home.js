import { Button, StyleSheet, Text, View, TextInput  } from 'react-native';
import { useState } from 'react';
import { getCrasApiCall } from '../services/CarService';
import { getRacesApiCall } from '../services/RaceService';

export default function Home({navigation}) {
    const printHandler = () => {
        navigation.navigate('LoginRegister');
    };
    const CarHandler = async () =>{
        try {
            console.log('start');
            var startTime = performance.now();
            var result = await getCrasApiCall(global.email);
            for (let i = 0; i < 99; i++) {
                result = await getCrasApiCall(global.email);
              }
           
            var endTime = performance.now()
            console.log(`Call to doSomething took ${endTime - startTime} milliseconds.`)
        } catch (error) {
            console.log(error);
        }
        console.log('end');
    }
    const fromId = 5;
    const toId = 8;

    const RaceHandler =async () => {
      try {
        console.log('start');
        var startTime = performance.now();
        var result = await getRacesApiCall(fromId, toId);
        var endTime = performance.now()
        console.log(`Call to doSomething took ${endTime - startTime} milliseconds.`)
    } catch (error) {
        console.log(error);
    }
    console.log('end');
  };

    console.log(global.IsloggedIn);
    if (global.IsloggedIn){
        return (
            <View style={styles.container}>
              <Text>Tis the home screen</Text>
              <Button title='gotToLoggedIn' onPress={printHandler}/>
              <Button title='getCars' onPress={CarHandler}/>
              <Button title='getRaces' onPress={RaceHandler}/>
            </View>
          );
    }else {
        navigation.navigate('LoginScreen');
        return (
            <View style={styles.container}>
              <Text>Tis the home screen</Text>
              <Button title='gotToLoggedIn' onPress={printHandler}/>
              <Button title='getCars' onPress={CarHandler}/>
              <Button title='getRaces' onPress={RaceHandler}/>
            </View>
          );
    }
  }

  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    textInput:{
        borderWidth: 1,
        borderColor: '#999',
        width: 250,
        margin: 15
    }
  });