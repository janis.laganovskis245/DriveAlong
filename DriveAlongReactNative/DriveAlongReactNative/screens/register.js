import { useState } from 'react';
import { StyleSheet, Text, View, Button, TextInput } from 'react-native';
import { SelectList } from 'react-native-dropdown-select-list';

import { registerApiCall, isDriverApiCall } from '../services/loginService';


export default function RegisterScreen({ navigation}) {

    const data = [
        { value: 'Driver', key: '1' },
        { value: 'User', key: '2' }
      ];
    
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [name, setName] = useState('');
    const [surname, setSurname] = useState('');    
    const [selected, setSelected] = useState("");


    const registerHandler = async () =>{
        const model =  {email: email, password: password, name: name, lastName: surname, isDriver: selected};
        const result = await registerApiCall(model);
        if(result){
            global.IsloggedIn = true;
            const isDriver = await isDriverApiCall(model);
            global.IsDriver = isDriver;
            global.Email = email;
            navigation.navigate('Home');
        };
    }

    return (
      <View style={styles.container}>
        <Text>Email:</Text>
        <TextInput style= {styles.textInput} onChangeText={(value)=> setEmail(value)}/> 
        <Text>Password:</Text>
        <TextInput style= {styles.textInput} onChangeText={(value)=> setPassword(value)}/> 
        <Text>Name:</Text>
        <TextInput style= {styles.textInput} onChangeText={(value)=> setName(value)}/> 
        <Text>Last name:</Text>
        <TextInput style= {styles.textInput} onChangeText={(value)=> setSurname(value)}/> 
        <SelectList
         setSelected={(val) => setSelected(val)} 
         data={data} 
         save="value"
        />
        <Button title='register' onPress={registerHandler}/>
      </View>
    );
  }
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    textInput:{
        borderWidth: 1,
        borderColor: '#999',
        width: 250,
        margin: 15
    }
  });