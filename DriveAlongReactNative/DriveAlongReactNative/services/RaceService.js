const webserviceApiAdress = 'https://drivealongv1.azurewebsites.net/';
export function getRacesApiCall(from, to) {
    return fetch(webserviceApiAdress + 'Race/GetRacesByLocations', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            StartLocaitonId: from,
            EndLocationId: to
        }),
      })
      .then((response) => response.json())
      .then((responseJson) => {
        return responseJson;
      })
      .catch((error) => {
        console.error(error);
      });
}
