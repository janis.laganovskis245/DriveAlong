const webserviceApiAdress = 'https://drivealongv1.azurewebsites.net/';
export function getCrasApiCall(email) {
    return fetch(webserviceApiAdress + 'Vehicle/GetVehicles', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          email
        }),
      })
      .then((response) => response.json())
      .then((responseJson) => {
        return responseJson;
      })
      .catch((error) => {
        console.error(error);
      });
}
