const webserviceApiAdress = 'https://drivealongv1.azurewebsites.net/';

export function loginApiCall(email, password) {
    return fetch(webserviceApiAdress + 'User/Login', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          Email: email,
          Password: password,
        }),
      })
      .then((response) => response.json())
      .then((responseJson) => {
        return responseJson.response;
      })
      .catch((error) => {
        console.error(error);
      });
}

export function isDriverApiCall(email) {
    return fetch(webserviceApiAdress + 'User/IsDriver', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          email
        }),
      })
      .then((response) => response.json())
      .then((responseJson) => {
        return responseJson;
      })
      .catch((error) => {
        console.error(error);
      });
}

export function registerApiCall(registerModel) {
    console.log(registerModel);
    const isDriver = registerModel.isDriver == 'Driver';
    console.log(isDriver);
    return fetch(webserviceApiAdress + 'User/Register', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            Name: registerModel.name,
            LastName: registerModel.lastName,
            Password: registerModel.password,
            Email: registerModel.email,
            IsDriver: (registerModel.isDriver == 'Driver')
        }),
      })
      .then((response) => response.json())
      .then((responseJson) => {
        return responseJson.response;
      })
      .catch((error) => {
        console.error(error);
      });
}

// public string Name { get; set; }
//         public string LastName { get; set; }
//         public string Password { get; set; }
//         public string Email { get; set; }
//         public bool IsDriver { get; set; }