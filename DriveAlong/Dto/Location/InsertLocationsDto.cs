﻿using System.ComponentModel.DataAnnotations;

namespace DriveAlong.Dto.Location
{
    public class InsertLocationsDto
    {
        [Required]
        public List<string> Names { get; set; }
    }
}
