﻿namespace DriveAlong.Dto.Location
{
    public class LocationDto
    {
        public int LocationId { get; set; }
        public string LocationName { get; set; }
    }
}
