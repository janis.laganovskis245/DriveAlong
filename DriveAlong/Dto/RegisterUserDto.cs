﻿using System.ComponentModel.DataAnnotations;

namespace DriveAlong.Dto
{
    public class RegisterUserDto
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public bool IsDriver { get; set; }
    }
}
