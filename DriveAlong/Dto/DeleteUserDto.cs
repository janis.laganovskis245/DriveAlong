﻿using System.ComponentModel.DataAnnotations;

namespace DriveAlong.Dto
{
    public class DeleteUserDto
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public bool IsDriver { get; set; }
    }
}
