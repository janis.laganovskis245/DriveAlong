﻿using System.ComponentModel.DataAnnotations;

namespace DriveAlong.Dto.Race
{
    public class GetRaceByLocationDto
    {
        [Required]
        public int StartLocaitonId { get; set; }
        [Required]
        public int EndLocationId { get; set; }
    }
}
