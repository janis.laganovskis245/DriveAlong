﻿using System.ComponentModel.DataAnnotations;

namespace DriveAlong.Dto.Race
{
    public class GetRaceByUserDto
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public bool IsDriver { get; set; }
    }
}
