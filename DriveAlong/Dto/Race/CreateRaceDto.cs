﻿using System.ComponentModel.DataAnnotations;

namespace DriveAlong.Dto.Race
{
    public class CreateRaceDto
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public int VehicleId { get; set; }
        [Required]
        public int AlreadyTakenSeats { get; set; }
        [Required]
        public int StartLocaitonId { get; set; }
        [Required]
        public int EndLocationId { get; set; }
        [Required]
        public DateTime StartDate { get; set; }
        [Required]
        public decimal DesiredPay { get; set; }
        [Required]
        public List<int> StopLocationIds { get; set; }
    }
}
