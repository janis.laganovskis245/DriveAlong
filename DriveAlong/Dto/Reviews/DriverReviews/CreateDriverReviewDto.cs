﻿using System.ComponentModel.DataAnnotations;

namespace DriveAlong.Dto.Reviews.DriverReviews
{
    public class CreateDriverReviewDto
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public int RaceId { get; set; }
        [Required]
        public int Rating { get; set; }
        [Required]
        public string Comment { get; set; }
    }
}
