﻿using System.ComponentModel.DataAnnotations;

namespace DriveAlong.Dto.Reviews.DriverReviews
{
    public class DriverReviewResponseDto
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public int Rating { get; set; }
        [Required]
        public string Comment { get; set; }
    }
}
