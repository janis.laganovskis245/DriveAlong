﻿using System.ComponentModel.DataAnnotations;

namespace DriveAlong.Dto.Reviews.DriverReviews
{
    public class UserReviewResponseDto
    {
        [Required]
        public string DriverName { get; set; }
        [Required]
        public int Rating { get; set; }
        [Required]
        public string Comment { get; set; }
    }
}
