﻿using System.ComponentModel.DataAnnotations;

namespace DriveAlong.Dto.Reviews.DriverReviews
{
    public class CreateUserReviewDto
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public int ParticipationId { get; set; }
        [Required]
        public int Rating { get; set; }
        [Required]
        public string Comment { get; set; }
    }
}
