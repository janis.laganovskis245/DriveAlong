﻿using System.ComponentModel.DataAnnotations;

namespace DriveAlong.Dto.Participation.ParticipationRequest
{
    public class CreateParticipationDenialDto
    {
        [Required]
        public int ParticipationRequestId { get; set; }
        [Required]
        public string Comment { get; set; }
    }
}
