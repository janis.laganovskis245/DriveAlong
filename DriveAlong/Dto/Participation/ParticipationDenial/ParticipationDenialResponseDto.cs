﻿using DriveAlong.Dto.Location;

namespace DriveAlong.Dto.Participation.ParticipationRequest
{
    public class ParticipationDenialResponseDto
    {
        public int RaceId { get; set; }
        public string Comment { get; set; }
        public LocationDto StartLocaiton { get; set; }
        public LocationDto EndLocation { get; set; }
        public DateTime StartDate { get; set; }
    }
}
