﻿using System.ComponentModel.DataAnnotations;

namespace DriveAlong.Dto.Participation.ParticipationRequest
{
    public class CreateParticipationDto
    {
        [Required]
        public int ParticipationRequestId { get; set; }
    }
}
