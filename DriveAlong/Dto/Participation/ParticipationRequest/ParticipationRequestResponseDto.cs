﻿namespace DriveAlong.Dto.Participation.ParticipationRequest
{
    public class ParticipationRequestResponseDto
    {
        public int ParticipationRequestId { get; set; }
        public int RaceId { get; set; }
        public string UserName { get; set; }
        public int UserId { get; set; }
        public decimal UserRating { get; set; }
    }
}
