﻿using System.ComponentModel.DataAnnotations;

namespace DriveAlong.Dto.Participation.ParticipationRequest
{
    public class CreateParticipationRequestDto
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public int RaceId { get; set; }
        public string Comment { get; set; }
    }
}
