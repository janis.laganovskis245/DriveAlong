﻿using System.ComponentModel.DataAnnotations;

namespace DriveAlong.Dto
{
    public class LoginUserDto
    {
        [Required]
        public string? Email { get; set; }
        [Required]
        public string? Password { get; set; }
    }
}
