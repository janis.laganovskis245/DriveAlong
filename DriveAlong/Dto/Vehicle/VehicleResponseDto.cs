﻿using System.ComponentModel.DataAnnotations;

namespace DriveAlong.Dto
{
    public class VehicleResponseDto
    {
        public int VehicleId { get; set; }
        public string Manufacturor { get; set; }
        public string Model { get; set; }
        public int Seats { get; set; }
        public int Year { get; set; }
    }
}
