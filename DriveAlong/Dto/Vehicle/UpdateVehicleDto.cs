﻿using System.ComponentModel.DataAnnotations;

namespace DriveAlong.Dto
{
    public class UpdateVehicleDto
    {
        [Required]
        public int VehicleId { get; set; }
        [Required]
        public string Manufacturor { get; set; }
        [Required]
        public string Model { get; set; }
        [Required]
        public int Seats { get; set; }
        [Required]
        public int Year { get; set; }
    }
}
