﻿using DriveAlong.DB;
using DriveAlong.DB.Models;

namespace DriveAlong.Services
{
    public class ReviewService 
    {      
        public static decimal GetDriverRating(int driverId)
        {
            using var db = new AppDbContext();
            var driverReviews = GetDriverReviewsByDriverId(db, driverId);
            return driverReviews.Count() == 0 ? 0 : driverReviews.Sum(x => x.Rating) / driverReviews.Count();
        }

        public static decimal GetUserRating(int userId)
        {
            using var db = new AppDbContext();
            var userReviews = GetUserReviewsByUserId(db, userId);
            return userReviews.Count() == 0 ? 0 : userReviews.Sum(x => x.Rating) / userReviews.Count();
        }

        private static List<DriverReview> GetDriverReviewsByDriverId(AppDbContext db,int driverId)
            => db.DriverReviews.Where(x => x.DriverId == driverId).ToList();

        private static List<DriverReview> GetDriverReviewsByUserId(AppDbContext db, int userId)
            => db.DriverReviews.Where(x => x.UserId == userId).ToList();

        private static List<UserReview> GetUserReviewsByDriverId(AppDbContext db, int driverId)
            => db.UserReviews.Where(x => x.DriverId == driverId).ToList();

        private static List<UserReview> GetUserReviewsByUserId(AppDbContext db, int userId)
            => db.UserReviews.Where(x => x.UserId == userId).ToList();
    }
}
