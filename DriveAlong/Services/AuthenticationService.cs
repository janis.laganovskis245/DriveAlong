﻿using DriveAlong.DB;
using DriveAlong.DB.Models;
using DriveAlong.Dto;
using FluentResults;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace DriveAlong.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly UserManager<UserAuth> _userManager;
        private readonly IConfiguration _configuration;

        public AuthenticationService(UserManager<UserAuth> userManager, IConfiguration configuration)
        {
            _userManager = userManager;
            _configuration = configuration;
        }

        public async Task<Result<string>> Register(RegisterUserDto request)
        {
            //var userByUsername = await _userManager.FindByNameAsync(request.UserName); // TODO => validate for existing accounts
            //if (userByUsername is not null)
            //{
            //    throw new ArgumentException($"User with username {request.UserName} already exists.");
            //}

            UserAuth baseUser = new UserAuth()
            {
                UserName = request.Name + request.LastName,
                IsDriver = request.IsDriver,
                Email = request.Email,
                SecurityStamp = Guid.NewGuid().ToString()
            };
            
            var result = await _userManager.CreateAsync(baseUser, request.Password);

            if (!result.Succeeded)
            {
                throw new ArgumentException($"Unable to register user {request.Email} errors: {GetErrorsText(result.Errors)}");
            }

            // insert new user db entry
            if (request.IsDriver)
            {
                using (var db = new AppDbContext())
                {
                    db.Drivers.Add(new Driver
                    {
                        Id = baseUser.Id,
                        Name = request.Name,
                        LastName = request.LastName,
                    });
                    db.SaveChanges();
                }
            }
            else
            {
                using (var db = new AppDbContext())
                {
                    db.DriveUsers.Add(new User
                    {
                        Id = baseUser.Id,
                        Name = request.Name,
                        LastName = request.LastName,
                    });
                    db.SaveChanges();
                }
            }
            return await Login(new LoginUserDto { Email = request.Email, Password = request.Password });
        }

        public async Task<Result<string>> Login(LoginUserDto request)
        {
            try
            {
                var user = await _userManager.FindByEmailAsync(request.Email);

                if (user is null || !await _userManager.CheckPasswordAsync(user, request.Password))
                {
                    throw new ArgumentException($"Unable to authenticate user {request.Email}");
                }

                var authClaims = new List<Claim>
                {
                    new(ClaimTypes.Name, user.UserName),
                    new(ClaimTypes.Email, user.Email),
                    new(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                };

                var userRoles = await _userManager.GetRolesAsync(user);
                authClaims.AddRange(userRoles.Select(userRole => new Claim(ClaimTypes.Role, userRole)));
                var token = GetToken(authClaims);
                return new JwtSecurityTokenHandler().WriteToken(token);
            }
            catch (Exception ex)
            {
                var shit = ex.Message;
            }
            throw new NotImplementedException();
        }

        private JwtSecurityToken GetToken(IEnumerable<Claim> authClaims)
        {
            var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));

            var token = new JwtSecurityToken(
                issuer: _configuration["JWT:ValidIssuer"],
                audience: _configuration["JWT:ValidAudience"],
                expires: DateTime.Now.AddHours(3),
                claims: authClaims,
                signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256));

            return token;
        }

        private string GetErrorsText(IEnumerable<IdentityError> errors)
        {
            return string.Join(", ", errors.Select(error => error.Description).ToArray());
        }

        public User GetUserByMail(string email)
        {
            var user = _userManager.FindByEmailAsync(email).Result;
            using var db = new AppDbContext();
            return db
                .DriveUsers
                .First(x => x.Id == user.Id);
        }

        public bool IsDriver(string email)
        {
            var user = _userManager.FindByEmailAsync(email).Result;
            using var db = new AppDbContext();
            return db
                .Drivers
                .Any(x => x.Id == user.Id);
        }

        public Driver GetDriverByMail(string email)
        {
            var user = _userManager.FindByEmailAsync(email).Result;
            using var db = new AppDbContext();
            return db
                .Drivers
                .First(x => x.Id == user.Id);
        }

        public void DeleteUser(DeleteUserDto request)
        {
            var user = _userManager.FindByEmailAsync(request.Email).Result;
            using var db = new AppDbContext();
            if (request.IsDriver)
            {
                db.Drivers.Remove(db.Drivers.Single(x => x.Id == user.Id));
            }
            else
            {
                db.DriveUsers.Remove(db.DriveUsers.Single(x => x.Id == user.Id));
            }
            db.SaveChanges();
        }
    }
}
