﻿using DriveAlong.DB.Models;

namespace DriveAlong.Services
{
    public interface IParticipationService
    {
        List<Participation> GetParticpationsByUserId(int userID);
        List<Race> GetParticipatedRacesByUserId(int userID);
        void RewokeParticipationRequest(int participationRequestId);
        void AproveParticipationRequest(int participationRequestId);
    }
}
