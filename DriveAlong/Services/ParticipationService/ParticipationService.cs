﻿using DriveAlong.DB;
using DriveAlong.DB.Models;

namespace DriveAlong.Services
{
    public class ParticipationService : IParticipationService
    {
        public ParticipationService()
        {
        }
      
        public List<Race> GetParticipatedRacesByUserId(int userID)
        {
            using var db = new AppDbContext();
            return db.Participations
                .Where(x => x.UserId == userID)
                .Select(x => db.Races.Single(y => y.RaceID == x.RaceId))
                .ToList();
        }

        public List<Participation> GetParticpationsByUserId(int userID)
        {
            using var db = new AppDbContext();
            return 
                db.Participations.Where(x => x.UserId == userID).ToList();
        }

        public void RewokeParticipationRequest(int participationRequestId)
        {
            using var db = new AppDbContext();
            db.ParticipationRequests.Remove(db.ParticipationRequests.Single(x => x.ParticipationRequestID == participationRequestId));
            db.SaveChanges();
        }

        public void AproveParticipationRequest(int participationRequestId)
        {
            using var db = new AppDbContext();
            var participationRequest = db.ParticipationRequests.Single(x => x.ParticipationRequestID == participationRequestId);
            var race = db.Races.Single(x => x.RaceID == participationRequest.RaceId);
            race.AvailableSeats--;
            db.ParticipationRequests.Remove(participationRequest);
            db.SaveChanges();
        }
    }
}
