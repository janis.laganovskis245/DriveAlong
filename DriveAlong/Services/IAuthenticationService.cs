﻿using DriveAlong.DB.Models;
using DriveAlong.Dto;
using FluentResults;

namespace DriveAlong.Services
{
    public interface IAuthenticationService
    {
        Task<Result<string>> Register(RegisterUserDto request);
        Task<Result<string>> Login(LoginUserDto request);
        User GetUserByMail(string email);
        bool IsDriver(string email);
        Driver GetDriverByMail(string email);
        void DeleteUser(DeleteUserDto request);
    }
}
