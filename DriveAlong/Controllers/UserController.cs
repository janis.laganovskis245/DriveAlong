using DriveAlong.DB;
using DriveAlong.Dto;
using DriveAlong.Dto.Participation.ParticipationRequest;
using DriveAlong.Extensions;
using DriveAlong.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DriveAlong.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IAuthenticationService _authenticationService;

        public UserController(IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }

        [AllowAnonymous]
        [HttpGet(Name = "GetStatus")]
        public bool GetStatus()
        {
            try
            {
                using (var db = new AppDbContext())
                {
                    var uniqueKey = db.Locations.Any();
                }
                return true;
            }
            catch(Exception)
            {
                return false;
            }       
        }

        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] LoginUserDto request)
        {
            var result = await _authenticationService.Login(request);
            var resultDto = result.ToResultDto();
            if (!resultDto.IsSuccess)
            {
                return BadRequest(resultDto);
            }
            return Ok(resultDto);
        }

        [HttpPost("IsDriver")]
        public async Task<bool> IsDriver([FromBody] string email)
        {
            return _authenticationService.IsDriver(email);
        }

        [AllowAnonymous]
        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] RegisterUserDto request)
        {
            var result = await _authenticationService.Register(request);
            var resultDto = result.ToResultDto();
            if (!resultDto.IsSuccess)
            {
                return BadRequest(resultDto);
            }
            return Ok(resultDto);
        }

        [HttpPost("DeleteUser")]
        public bool DeleteUser([FromBody] DeleteUserDto request)
        {
            try
            {
                _authenticationService.DeleteUser(request);
                return true;
            } 
            catch(Exception)
            {
                return false;
            }
        }
    }
}