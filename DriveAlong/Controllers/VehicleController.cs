using DriveAlong.DB;
using DriveAlong.DB.Models;
using DriveAlong.Dto;
using DriveAlong.Services;
using Microsoft.AspNetCore.Mvc;

namespace DriveAlong.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VehicleController : ControllerBase
    {
        private readonly IAuthenticationService _authenticationService;

        public VehicleController(IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }

        [HttpPost("CreateVehicle")]
        public bool CreateVehicle(CreateVehicleDto request)
        {
            try
            {
                using (var db = new AppDbContext())
                {
                    db.Vehicles.Add(new Vehicle
                    {
                        Manufacturor = request.Manufacturor,
                        Model = request.Model,
                        Seats = request.Seats,
                        Year = request.Year,
                        DriverId = _authenticationService.GetDriverByMail(request.Email).DriverID
                    });
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        [HttpPost("UpdateVehicle")]
        public bool UpdateVehicle(UpdateVehicleDto request)
        {
            try
            {
                using (var db = new AppDbContext())
                {
                    var vehicle = db
                        .Vehicles
                        .Single(x => x.VehicleID == request.VehicleId);
                    vehicle.Manufacturor = request.Manufacturor;
                    vehicle.Model = request.Model;
                    vehicle.Seats = request.Seats;
                    vehicle.Year = request.Year;
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        [HttpPost("GetVehicles")]
        public List<VehicleResponseDto> GetVehicles([FromBody]string email)
        {
            var returnList = new List<VehicleResponseDto>();
            var driverId = _authenticationService.GetDriverByMail(email).DriverID;
            using var db = new AppDbContext();
            var dbVehicles = db
                .Vehicles
                .Where(x => x.DriverId == driverId);
            foreach(var dbvehicle in dbVehicles)
            {
                returnList.Add(new VehicleResponseDto
                {
                    Year = dbvehicle.Year,
                    Manufacturor = dbvehicle.Manufacturor,
                    Model = dbvehicle.Model,
                    Seats = dbvehicle.Seats,
                    VehicleId = dbvehicle.VehicleID
                });
            }
            return returnList;
        }

        [HttpPost("DeleteVehicle")]
        public bool DeleteVehicle([FromBody] int vehicleId)
        {
            try
            {
                using var db = new AppDbContext();
                db.Vehicles.Remove(db.Vehicles.Single(x => x.VehicleID == vehicleId));
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}