using DriveAlong.DB;
using DriveAlong.DB.Models;
using DriveAlong.Dto.Reviews.DriverReviews;
using DriveAlong.Services;
using Microsoft.AspNetCore.Mvc;

namespace DriveAlong.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ReviewController : ControllerBase
    {
        private readonly IAuthenticationService _authenticationService;

        public ReviewController(IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }

        [HttpPost("CreateDriverReview")]
        public bool CreateDriverReview(CreateDriverReviewDto request)
        {
            try
            {
                using var db = new AppDbContext();
                var user = _authenticationService.GetUserByMail(request.Email);
                var driverId = db.Races.Single(x => x.RaceID == request.RaceId).DriverId;
                db.DriverReviews.Add(new DriverReview
                {
                    DriverId = driverId,  
                    UserId = user.UserID,
                    Comment = request.Comment,
                    Rating= request.Rating                    
                });
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        [HttpPost("GetDriverReviews")]
        public List<DriverReviewResponseDto> GetDriverReviews(int driverId)
        {
            using var db = new AppDbContext();
             
            return db.DriverReviews
                .Where(x => x.DriverId == driverId)
                .Select(x => ConvertToDriverReviewResponse(db, x))
                .ToList();
        }       

        [HttpPost("CreateUserReview")]
        public bool CreateUserReview(CreateUserReviewDto request)
        {
            try
            {
                using var db = new AppDbContext();
                var driver = _authenticationService.GetDriverByMail(request.Email);
                var userId = db.Participations.Single(x => x.ParticipationID == request.ParticipationId).UserId;
                db.UserReviews.Add(new UserReview
                {
                    DriverId = driver.DriverID,
                    UserId = userId,
                    Comment = request.Comment,
                    Rating = request.Rating
                });
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        [HttpPost("GetUserReviews")]
        public List<UserReviewResponseDto> GetUserReviews(int userId)
        {
            using var db = new AppDbContext();

            return db.UserReviews
                .Where(x => x.UserId == userId)
                .Select(x => ConvertToUserReviewResponse(db, x))
                .ToList();
        }

        private DriverReviewResponseDto ConvertToDriverReviewResponse(AppDbContext db, DriverReview x)
        {
            var user = db.DriveUsers.Single(y => y.UserID == x.UserId);
            return new DriverReviewResponseDto
            {
                Comment = x.Comment,
                Rating = x.Rating,
                UserName = user.Name + " " + user.LastName
            };
        }

        private UserReviewResponseDto ConvertToUserReviewResponse(AppDbContext db, UserReview x)
        {
            var driver = db.Drivers.Single(y => y.DriverID == x.DriverId);
            return new UserReviewResponseDto
            {
                Comment = x.Comment,
                Rating = x.Rating,
                DriverName = driver.Name + " " + driver.LastName
            };
        }
    }
}