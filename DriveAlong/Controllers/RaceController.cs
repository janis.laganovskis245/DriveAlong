using DriveAlong.DB;
using DriveAlong.DB.Models;
using DriveAlong.Dto.Location;
using DriveAlong.Dto.Race;
using DriveAlong.Extensions;
using DriveAlong.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace DriveAlong.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RaceController : ControllerBase
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly IParticipationService _participationServicce;

        public RaceController(IAuthenticationService authenticationService, IParticipationService participationService)
        {
            _authenticationService = authenticationService;
            _participationServicce = participationService;
        }

        [AllowAnonymous]
        [HttpPost("CreateRaceMass")]
        public bool CreateRaceMass(CreateRaceDto request)
        {
            try
            {
                using (var db = new AppDbContext())
                {
                    var driver = _authenticationService.GetDriverByMail(request.Email);
                    var vehicle = db.Vehicles.Single(x => x.VehicleID == request.VehicleId);
                    for (var i = 0; i<100; i++)
                    {
                        var newRace = new Race
                        {
                            AvailableSeats = vehicle.Seats - request.AlreadyTakenSeats,
                            DriverId = driver.DriverID,
                            StartLocaitonId = request.StartLocaitonId,
                            EndLocaitonId = request.EndLocationId,
                            Start = request.StartDate.SetKindUtc(),
                            DesiredPay = request.DesiredPay,
                            VehicleID = request.VehicleId
                        };
                        db.Races.Add(newRace);
                        db.SaveChanges();
                        CreateStops(db, request.StopLocationIds, newRace.RaceID);
                    }
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        [HttpPost("CreateRace")]
        public bool CreateRace(CreateRaceDto request)
        {
            try
            {
                using (var db = new AppDbContext())
                {
                    var driver = _authenticationService.GetDriverByMail(request.Email);
                    var vehicle = db.Vehicles.Single(x => x.VehicleID == request.VehicleId);
                    var newRace = new Race
                    {
                        AvailableSeats = vehicle.Seats - request.AlreadyTakenSeats,
                        DriverId = driver.DriverID,
                        StartLocaitonId = request.StartLocaitonId,
                        EndLocaitonId = request.EndLocationId,
                        Start = request.StartDate.SetKindUtc(),
                        DesiredPay = request.DesiredPay,
                        VehicleID = request.VehicleId
                    };
                    db.Races.Add(newRace);
                    db.SaveChanges();
                    CreateStops(db, request.StopLocationIds, newRace.RaceID);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void CreateStops(AppDbContext db, List<int> stopLocationIds, int raceId)
        {
            db.Stops.AddRange(stopLocationIds.Select(stop => new Stop
            {
                LocationId = stop,
                RaceId = raceId
            }));
            db.SaveChanges();
        }

        [HttpPost("GetRacesByLocations")]
        public List<RaceResponseDto> GetRacesByLocations(GetRaceByLocationDto request)
        {
            var returnList = new List<RaceResponseDto>();
            using var db = new AppDbContext();

            var matchingLocaitons = db.Races.Where(x => x.StartLocaitonId == request.StartLocaitonId).ToList();
            foreach (var race in matchingLocaitons)
            {
                if (race.EndLocaitonId == request.EndLocationId)
                {
                    returnList.Add(ConvertToReturnModel(db, race));
                    continue;
                }

                var stops = db
                    .Stops
                    .Where(x => x.RaceId == race.RaceID)
                    .ToList();
                if (stops.Any(x => x.LocationId == request.EndLocationId))
                {
                    returnList.Add(ConvertToReturnModel(db, race));
                }
            }        
            return returnList;
        }

        [HttpPost("GetRacesByUser")]
        public List<RaceResponseDto> GetRacesByUser(GetRaceByUserDto request)
        {
            try
            {
                using var db = new AppDbContext();
                if (request.IsDriver)
                {
                    var driverId = _authenticationService.GetDriverByMail(request.Email).DriverID;
                    var races = db.Races
                        .Where(x => x.DriverId == driverId)
                        .ToList();
                    return races
                        .Select(x => ConvertToReturnModel(db, x))
                        .ToList();
                }
                
                return 
                    _participationServicce
                        .GetParticipatedRacesByUserId(_authenticationService.GetUserByMail(request.Email).UserID)
                        .Select(x => ConvertToReturnModel(db, x))
                        .ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }

        [HttpPost("GetRaceById")]
        public RaceResponseDto GetRaceById(int raceId)
        {
            try
            {
                using var db = new AppDbContext();
                return
                    ConvertToReturnModel(db, db.Races.Single(x => x.RaceID == raceId));
            }
            catch (Exception)
            {
                return null;
            }
        }

        private static RaceResponseDto ConvertToReturnModel(AppDbContext db, Race race)
        {
            var vehicle = db.Vehicles.Single(x => x.VehicleID == race.VehicleID);

            return new RaceResponseDto
            {
                DesiredPay = race.DesiredPay,
                DriverRating = ReviewService.GetDriverRating(race.DriverId),
                StartLocaiton = new LocationDto
                {
                    LocationId = race.StartLocaitonId.Value,
                    LocationName = db.Locations.Single(x => x.LocationID == race.StartLocaitonId).Name
                },
                EndLocation = new LocationDto
                {
                    LocationId = race.EndLocaitonId.Value,
                    LocationName = db.Locations.Single(x => x.LocationID == race.EndLocaitonId).Name
                },
                FreeSeats = race.AvailableSeats,
                StartDate = race.Start,
                Stops = GetConvertedStopsForRaceStops(db, race.RaceID),
                VehicleManufacturor = vehicle.Manufacturor,
                VehicleModel = vehicle.Model,
                VehicleYear = vehicle.Year,
                RaceId = race.RaceID,
                DriverId = race.DriverId
            };
        }

        private static List<LocationDto> GetConvertedStopsForRaceStops(AppDbContext db, int raceID)
        {
            var stops = db.Stops.Where(x => x.RaceId == raceID);
            return
                stops.Select(stop => new LocationDto
                {
                    LocationId = stop.LocationId,
                    LocationName = db.Locations.Single(x => x.LocationID == stop.LocationId).Name
                }).ToList();
        }       
    }
}