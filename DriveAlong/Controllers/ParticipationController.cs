using DriveAlong.DB;
using DriveAlong.DB.Models;
using DriveAlong.Dto.Location;
using DriveAlong.Dto.Participation.ParticipationRequest;
using DriveAlong.Services;
using Microsoft.AspNetCore.Mvc;

namespace DriveAlong.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ParticipationController : ControllerBase
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly IParticipationService _participationServicce;

        public ParticipationController(IAuthenticationService authenticationService, IParticipationService participationService)
        {
            _authenticationService = authenticationService;
            _participationServicce = participationService;
        }

        [HttpPost("CreateParticipationRequest")]
        public bool CreateParticipationRequest(CreateParticipationRequestDto request)
        {
            try
            {
                using var db = new AppDbContext();
                var user = _authenticationService.GetUserByMail(request.Email);
                db.ParticipationRequests.Add(new ParticipationRequest
                {
                    RaceId = request.RaceId,
                    UserComment = request.Comment,
                    UserId = user.UserID
                });
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        [HttpPost("GetParticipationRequestsByRace")]
        public List<ParticipationRequestResponseDto> GetParticipationRequestsByRace([FromBody]int raceId)
        {
            using var db = new AppDbContext();
            var races = db.ParticipationRequests
                .Where(x => x.RaceId == raceId)
                .ToList();
            return
                races
                .Select(x => ConvertToParticipationRequestResponse(db, x))
                .ToList();
        }     

        [HttpPost("GetParticipationRequestsByUser")]
        public List<ParticipationRequestResponseDto> GetParticipationRequestsByUser([FromBody] string email)
        {
            var user = _authenticationService.GetUserByMail(email);
            using var db = new AppDbContext();

            return db.ParticipationRequests
                .Where(x => x.UserId == user.UserID)
                .Select(x => ConvertToParticipationRequestResponse(db, x))
                .ToList();
        }

        [HttpPost("CreateParticipationDenial")]
        public bool CreateParticipationDenial(CreateParticipationDenialDto request)
        {
            try
            {
                using var db = new AppDbContext();
                var participationRequest = db.ParticipationRequests.Single(x => x.ParticipationRequestID == request.ParticipationRequestId);
              
                db.ParticipationDenials.Add(new ParticipationDenialResponse
                {
                    RaceId = participationRequest.RaceId,
                    DriverComment = request.Comment,
                    UserId = participationRequest.UserId
                });
                db.SaveChanges();
                _participationServicce.RewokeParticipationRequest(request.ParticipationRequestId);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        [HttpPost("GetParticipationDenialsByUser")]
        public List<ParticipationDenialResponseDto> GetParticipationDenialsByUser(string email)
        {
            var user = _authenticationService.GetUserByMail(email);
            using var db = new AppDbContext();

            return db.ParticipationDenials
                .Where(x => x.UserId == user.UserID)
                .Select(x => ConvertToParticipationDenialResponse(db, x))
                .ToList();
        }

        [HttpPost("CreateParticipation")]
        public bool CreateParticipation(CreateParticipationDto request)
        {
            try
            {
                using var db = new AppDbContext();
                var participationRequest = db.ParticipationRequests.Single(x => x.ParticipationRequestID == request.ParticipationRequestId);

                db.Participations.Add(new Participation
                {
                    RaceId = participationRequest.RaceId,
                    UserId = participationRequest.UserId                    
                });
                db.SaveChanges();
                _participationServicce.AproveParticipationRequest(request.ParticipationRequestId);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        [HttpPost("GetParticipationsByUser")]
        public List<ParticipationResponseDto> GetParticipationsByUser(string email)
        {
            var user = _authenticationService.GetUserByMail(email);
            using var db = new AppDbContext();
            var races = db.Participations
                .Where(x => x.UserId == user.UserID)
                .ToList();
            return
                races
                .Select(x => ConvertToParticipationResponse(db, x))
                .ToList();
        }

        private static ParticipationRequestResponseDto ConvertToParticipationRequestResponse(AppDbContext db, ParticipationRequest x)
        {
            var user = db.DriveUsers.Single(x => x.UserID == x.UserID);
            return new ParticipationRequestResponseDto
            {
                ParticipationRequestId = x.ParticipationRequestID,
                RaceId = x.RaceId,
                UserName = user.Name + " " + user.LastName,
                UserRating = ReviewService.GetUserRating(user.UserID),
                UserId = user.UserID
            };
        }

        private ParticipationDenialResponseDto ConvertToParticipationDenialResponse(AppDbContext db, ParticipationDenialResponse x)
        {
            var race = db.Races.Single(x => x.RaceID == x.RaceID);
            return new ParticipationDenialResponseDto
            {
                Comment = x.DriverComment,
                RaceId = x.RaceId,
                StartLocaiton = new LocationDto
                {
                    LocationId = race.StartLocaitonId.Value,
                    LocationName = db.Locations.Single(x => x.LocationID == race.StartLocaitonId).Name
                },
                EndLocation = new LocationDto
                {
                    LocationId = race.EndLocaitonId.Value,
                    LocationName = db.Locations.Single(x => x.LocationID == race.EndLocaitonId).Name
                },
                StartDate = race.Start
            };
        }

        private ParticipationResponseDto ConvertToParticipationResponse(AppDbContext db, Participation x)
        {
            var race = db.Races.Single(x => x.RaceID == x.RaceID);
            return new ParticipationResponseDto
            {
                RaceId = x.RaceId,
                StartLocaiton = new LocationDto
                {
                    LocationId = race.StartLocaitonId.Value,
                    LocationName = db.Locations.Single(x => x.LocationID == race.StartLocaitonId).Name
                },
                EndLocation = new LocationDto
                {
                    LocationId = race.EndLocaitonId.Value,
                    LocationName = db.Locations.Single(x => x.LocationID == race.EndLocaitonId).Name
                },
                StartDate = race.Start
            };
        }
    }
}