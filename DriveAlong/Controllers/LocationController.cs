using DriveAlong.DB;
using DriveAlong.DB.Models;
using DriveAlong.Dto.Location;
using DriveAlong.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DriveAlong.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LocationController : ControllerBase
    {
        public LocationController()
        {
        }

        [AllowAnonymous]
        [HttpPost("InsertLocations")]
        public bool InsertLocations(InsertLocationsDto request)
        {
            try
            {
                using var db = new AppDbContext();
                var existingLocations = db.Locations.ToList();
                foreach (var location in request.Names)
                {
                    if (!existingLocations.Any(x => x.Name.Is(location)))
                        db.Locations.Add(new Location { Name = location });
                }
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        [HttpGet("GetLocations")]
        public List<LocationDto> GetLocations()
        {
            try
            {
                using var db = new AppDbContext();
                return db.Locations
                    .Select(x => new LocationDto 
                    {
                        LocationId = x.LocationID, 
                        LocationName = x.Name
                    })
                    .ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}