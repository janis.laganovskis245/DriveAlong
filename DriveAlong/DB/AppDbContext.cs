﻿using DriveAlong.DB.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DriveAlong.DB
{
    public class AppDbContext : IdentityDbContext<UserAuth>
    {
        public DbSet<UserAuth> UserAuths { get; set; }
        public DbSet<Driver> Drivers { get; set; }
        public DbSet<User> DriveUsers { get; set; }
        public DbSet<Race> Races { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<DriverReview> DriverReviews { get; set; }
        public DbSet<Participation> Participations { get; set; }
        public DbSet<ParticipationDenialResponse> ParticipationDenials { get; set; }
        public DbSet<ParticipationRequest> ParticipationRequests { get; set; }
        public DbSet<Stop> Stops { get; set; }
        public DbSet<UserReview> UserReviews { get; set; }
        public DbSet<Vehicle> Vehicles { get; set; }

        protected readonly IConfiguration Configuration;

        public AppDbContext()
        {
        }

        public AppDbContext(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            // connect to postgres with connection string from app settings
            options.UseNpgsql("Host=drivealong.postgres.database.azure.com; Database=postgres; Username=janis@drivealong;Password=Qwerty1@3456");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Race>()
                .HasOne(x => x.StartLocation)
                .WithMany(x => x.StartLocations)
                .HasForeignKey(x => x.StartLocaitonId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            modelBuilder.Entity<Race>()
               .HasOne(x => x.EndLocation)
               .WithMany(x => x.EndLocations)
               .HasForeignKey(x => x.EndLocaitonId)
               .OnDelete(DeleteBehavior.ClientSetNull);
        }     
    }
}
