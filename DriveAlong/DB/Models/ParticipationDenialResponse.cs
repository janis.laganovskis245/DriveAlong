﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DriveAlong.DB.Models
{
    public class ParticipationDenialResponse
    {
        [Key]
        public int ParticipationDenialResponseID { get; set; }

        [Required]
        public int RaceId { get; set; }
        [Required]
        [ForeignKey("RaceId")]
        public Race Race { get; set; }

        [Required]
        public int UserId { get; set; }
        [Required]
        [ForeignKey("UserId")]
        public User user { get; set; }

        public string DriverComment { get; set; }
    }
}
