﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DriveAlong.DB.Models
{
    public class User
    {
        [Key]
        public int UserID { get; set; }

        [Required]
        public string Id { get; set; }
        [Required]
        [ForeignKey("Id")]
        public UserAuth UserAuth { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string LastName { get; set; }

        public List<ParticipationRequest> Requests { get; set; }
        public List<Participation> Participations { get; set; }
        public List<ParticipationDenialResponse> Denials { get; set; }
        public List<UserReview> UserReviews { get; set; }
        public List<DriverReview> DriverReviews { get; set; }
    }
}
