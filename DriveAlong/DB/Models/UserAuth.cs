﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace DriveAlong.DB.Models
{
    public class UserAuth : IdentityUser
    {
        [Required]
        public bool IsDriver { get; set; }
        public User? User { get; set; }
        public Driver? Driver { get; set; }
    }
}
