﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DriveAlong.DB.Models
{
    public class Vehicle
    {
        [Key]
        public int VehicleID { get; set; }

        [Required]
        public int DriverId { get; set; }
        [Required]
        [ForeignKey("DriverId")]
        public Driver Driver { get; set; }

        [Required]
        public string Manufacturor { get; set; }

        [Required]
        public string Model { get; set; }

        [Required]
        public int Seats { get; set; }

        [Required]
        public int Year { get; set; }

        public List<Race> Races { get; set; }
    }
}
