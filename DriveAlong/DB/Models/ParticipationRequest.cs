﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DriveAlong.DB.Models
{
    public class ParticipationRequest
    {
        [Key]
        public int ParticipationRequestID { get; set; }

        [Required]
        public int RaceId { get; set; }
        [Required]
        [ForeignKey("RaceId")]
        public Race Race { get; set; }

        [Required]
        public int UserId { get; set; }
        [Required]
        [ForeignKey("UserId")]
        public User user { get; set; }

        public string UserComment { get; set; }
    }
}
