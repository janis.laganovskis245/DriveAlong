﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DriveAlong.DB.Models
{
    public class DriverReview
    {
        [Key]
        public int DriverReviewID { get; set; }

        [Required]
        public int UserId { get; set; }
        [Required]
        [ForeignKey("UserId")]
        public User User { get; set; }

        [Required]
        public int DriverId { get; set; }
        [Required]
        [ForeignKey("DriverId")]
        public Driver Driver { get; set; }

        [Required]
        public int Rating { get; set; }
        public string Comment { get; set; }
    }
}
