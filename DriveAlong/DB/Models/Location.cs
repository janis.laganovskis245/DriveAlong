﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DriveAlong.DB.Models
{
    public class Location
    {
        [Key]
        public int LocationID { get; set; }

        [Required]
        public string Name { get; set; }

        public List<Race> StartLocations { get; set; }
        public List<Race> EndLocations { get; set; }

        public List<Stop> Stops { get; set; }
    }
}
