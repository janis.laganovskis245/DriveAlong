﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DriveAlong.DB.Models
{
    public class Stop
    {
        [Key]
        public int StopID { get; set; }

        [Required]
        public int RaceId { get; set; }
        [Required]
        [ForeignKey("RaceId")]
        public Race Race { get; set; }


        [Required]
        public int LocationId { get; set; }
        [Required]
        [ForeignKey("LocationId")]
        public Location Location { get; set; }

    }
}
