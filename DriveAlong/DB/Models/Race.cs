﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DriveAlong.DB.Models
{
    public class Race
    {
        [Key]
        public int RaceID { get; set; }

        [Required]
        public int DriverId { get; set; }
        [Required]
        [ForeignKey("DriverId")]
        public Driver Driver { get; set; }

        public int? StartLocaitonId { get; set; }
        public Location StartLocation { get; set; }

        public int? EndLocaitonId { get; set; }
        public Location EndLocation { get; set; }

        [Required]
        public int VehicleID { get; set; }
        [Required]
        [ForeignKey("VehicleID")]
        public Vehicle Vehicle { get; set; }

        [Required]
        public DateTime Start { get; set; }

        [Required]
        public int AvailableSeats { get; set; }

        [Required]
        public decimal DesiredPay { get; set; }

        public List<Stop> Stops { get; set; }
        public List<ParticipationRequest> Requests { get; set; }
        public List<Participation> Participations { get; set; }
        public List<ParticipationDenialResponse> Denials { get; set; }
    }
}
