﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DriveAlong.DB.Models
{
    public class Driver
    {
        [Key]
        public int DriverID { get; set; }
        [Required]
        public string Id { get; set; }
        [Required]
        [ForeignKey("Id")]
        public UserAuth UserAuth { get; set; }

        [Required]
        public string Name { get; set; }
        [Required]
        public string LastName { get; set; }

        public List<Vehicle> Vehicles { get; set; }
        public List<Race> Races { get; set; }
        public List<UserReview> UserReviews { get; set; }
        public List<DriverReview> DriverReviews { get; set; }
    }
}
