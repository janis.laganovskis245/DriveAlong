﻿using DriveAlong.Dto;
using FluentResults;

namespace DriveAlong.Extensions
{
    public static class ResultExtension
    {
        public static ResultDto<TResponse> ToResultDto<TResponse>(this Result<TResponse> result)
        {
            var errorMessages = result.Errors?.Select(error => error.Message);

            return new ResultDto<TResponse>(result.IsSuccess, result.ValueOrDefault, errorMessages);
        }
    }
}
