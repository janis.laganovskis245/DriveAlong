﻿namespace DriveAlong.Extensions
{
    public static class StringExtensions
    {
        public static bool Is(this string input, string comparison)
            => input.ToLowerInvariant().Equals(comparison.ToLowerInvariant());
    }
}
