﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DriveAlong.Migrations
{
    /// <inheritdoc />
    public partial class InitialCorrections : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Rating",
                table: "UserReviews",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "VehicleID",
                table: "Races",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Rating",
                table: "DriverReviews",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Races_VehicleID",
                table: "Races",
                column: "VehicleID");

            migrationBuilder.AddForeignKey(
                name: "FK_Races_Vehicles_VehicleID",
                table: "Races",
                column: "VehicleID",
                principalTable: "Vehicles",
                principalColumn: "VehicleID",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Races_Vehicles_VehicleID",
                table: "Races");

            migrationBuilder.DropIndex(
                name: "IX_Races_VehicleID",
                table: "Races");

            migrationBuilder.DropColumn(
                name: "Rating",
                table: "UserReviews");

            migrationBuilder.DropColumn(
                name: "VehicleID",
                table: "Races");

            migrationBuilder.DropColumn(
                name: "Rating",
                table: "DriverReviews");
        }
    }
}
